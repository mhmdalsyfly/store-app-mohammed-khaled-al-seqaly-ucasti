import 'package:get/get.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';

class UsernameController extends GetxController {
  RxString UserName = ''.obs;

  static UsernameController get to => Get.find();


  @override
  void onInit() {
    UserName.value = SharedPrefController().getNameUser;
    super.onInit();
  }

  changeUserName(String name) {
    UserName.value = name;
  }
}
