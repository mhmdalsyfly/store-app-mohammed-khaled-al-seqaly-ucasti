import 'package:get/get.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';

class LanguageController extends GetxController {
  RxString _locale = 'en'.obs;

  static LanguageController get to => Get.find();

  @override
  void onInit() {
    _locale.value = SharedPrefController().getLangCode;
    super.onInit();
  }

  RxString get locale => this._locale;

  changeLocale(String locale) {
    this._locale.value = locale;
  }
}
