import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/category.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_listview_category.dart';
import 'package:store_app/widgets/my_basket_full_icon.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'category_screen.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        backgroundColor: AppColors.TRANSPARENT,
        centerTitle: true,
        elevation: 0,
        title: MyText(
          title: AppLocalizations.of(context)!.categories,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        leading: Container(
          margin: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () {
              Get.back();
            },
          ),
        ),
        actions: [
          MyBasketFullIcon(
            onTap: () {
              Get.toNamed('/cart_screen');
            },
          ),
        ],
      ),
      body: Obx(() {
        List<Category> listCategory = CategoriesController.to.listCategory;
        if (listCategory.length > 0) {
          return GridView.builder(
            padding: EdgeInsets.only(top: SizeConfig.scaleHeight(20) ),
            itemCount: listCategory.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio:
                  SizeConfig.scaleWidth(154) / SizeConfig.scaleHeight(160),
              crossAxisSpacing: SizeConfig.scaleWidth(20),
              mainAxisSpacing: SizeConfig.scaleHeight(20),
            ),
            itemBuilder: (context, index) {
              return ItemListViewCategory(
                category: listCategory[index],
                onTap: () {
                  Get.to(CategoryScreen(category: listCategory[index]));
                },
                height: 100,
                width: 100,
                fontTitle: 18,
                fontWeight: FontWeight.w500,
              );
            },
          );
        } else {
          return Container(
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.center,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
            ),
          );
        }
      }),
    );
  }
}
