import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/model/category.dart';
import 'package:store_app/model/sub_category.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_basket_full_icon.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_screen_list_products.dart';
import 'package:store_app/widgets/my_tapbar.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CategoryScreen extends StatefulWidget {
  Category category;

  CategoryScreen({required this.category});

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> with MixinSittingsApp {
  List<Widget> listWidgets = [];
  List<Tab> listTabs = [];

  @override
  void didChangeDependencies() {
    CategoriesController.to.getSubCategories(widget.category.id.toString());
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        backgroundColor: AppColors.TRANSPARENT,
        centerTitle: true,
        elevation: 0,
        title: MyText(
          title: SharedPrefController().getLangCode == 'en'
              ? widget.category.nameEn
              : widget.category.nameAr,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        leading: Container(
          margin: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () {
              Get.back();
            },
          ),
        ),
        actions: [
          MyBasketFullIcon(
            onTap: () {
              Get.toNamed('/cart_screen');
            },
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(20)),
            SizedBox(
              width: double.infinity,
              child: MyText(
                title: AppLocalizations.of(context)!.subCategories,
                color: AppColors.TEXT_Bold,
                fontWeight: FontWeight.bold,
                fontSize: 18,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(16)),
            Expanded(
              child: FutureBuilder<List<SubCategory>>(
                future: CategoriesController(context: context).getSubCategories(
                  widget.category.id.toString(),
                ),
                builder: (context, snapshot) {
                  List<SubCategory> listSubCategories = snapshot.data ?? [];
                  listWidgets.clear();
                  listTabs.clear();
                  if (snapshot.hasData) {
                    for (int i = 0; i < listSubCategories.length; i++) {
                      listWidgets.add(MyScreenListProduct(
                        id: listSubCategories[i].id.toString(),
                      ));
                      listTabs.add(Tab(
                        text: SharedPrefController().getLangCode == 'en'
                            ? listSubCategories[i].nameEn
                            : listSubCategories[i].nameAr,
                      ));
                    }
                    return MyTapBar(
                      listWidgets: listWidgets,
                      listTabs: listTabs,
                    );
                  } else if (snapshot.hasError) {
                    return Center(
                      child: MyText(
                        title: 'No Data',
                      ),
                    );
                  } else
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                      ),
                    );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
/*
listWidgets.clear();
                  listTabs.clear();
                  print(CategoriesController.to.listSubCategory.length);
                  if (CategoriesController.to.listSubCategory.length > 0) {
                    for (int i = 0;
                        i < CategoriesController.to.listSubCategory.length;
                        i++) {
                      listWidgets.add(MyScreenListProduct(
                        id: CategoriesController.to.listSubCategory[i].id
                            .toString(),
                      ));
                      listTabs.add(Tab(
                        text: CategoriesController.to.listSubCategory[i].nameEn,
                      ));
                    }
                    return MyTapBar(
                      listWidgets: listWidgets,
                      listTabs: listTabs,
                    );
                  } else
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                      ),
                    );
* */
