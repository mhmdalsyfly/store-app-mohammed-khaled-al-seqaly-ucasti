import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/screens/products/product_details_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_grid_view_product.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';

class ShowAllProduct extends StatelessWidget {
  String title;

  ShowAllProduct({required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: title,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
        ),
        child: FutureBuilder<List<Product>>(
          future: 'Latest Products' == title
              ? CategoriesController(context: context).getProducts()
              : CategoriesController(context: context)
                  .getProducts(type: 'famous_products'),
          builder: (context, snapshot) {
            List<Product> listFamousProducts = snapshot.data ?? [];
            if (snapshot.hasData) {
              return GridView.builder(
                padding: EdgeInsets.zero,
                itemCount: listFamousProducts.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio:
                      SizeConfig.scaleWidth(156) / SizeConfig.scaleHeight(175),
                  crossAxisSpacing: SizeConfig.scaleWidth(14),
                  mainAxisSpacing: SizeConfig.scaleHeight(16),
                ),
                itemBuilder: (context, index) {
                  int? offer = listFamousProducts[index].offerPrice;
                  return ItemGridViewProduct(
                    offer: offer == null ? 0 : offer,
                    product: listFamousProducts[index],
                    onTap: () {
                      Get.to(
                        ProductDetailsScreen(
                          idProduct: listFamousProducts[index].id.toString(),
                        ),
                      );
                    },
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
