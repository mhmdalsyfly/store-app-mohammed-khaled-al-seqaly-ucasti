import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/product_offer.dart';
import 'package:store_app/screens/products/product_details_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_grid_view_product_offer.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProductsOfferScreen extends StatelessWidget {
  const ProductsOfferScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.productsOffers,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: FutureBuilder<List<ProductOffer>>(
          future: CategoriesController(context: context).getProductsOffer(),
          builder: (context, snapshot) {
            List<ProductOffer> listOffersProducts = snapshot.data ?? [];
            if (snapshot.hasData) {
              return Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(20),
                  vertical: SizeConfig.scaleHeight(10),
                ),
                child: GridView.builder(
                  padding: EdgeInsets.zero,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: listOffersProducts.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: SizeConfig.scaleWidth(156) /
                        SizeConfig.scaleHeight(175),
                    crossAxisSpacing: SizeConfig.scaleWidth(14),
                    mainAxisSpacing: SizeConfig.scaleHeight(16),
                  ),
                  itemBuilder: (context, index) {
                    return ItemGridViewProductOffer(
                      offer: listOffersProducts[index].discountPrice,
                      productOffer: listOffersProducts[index],
                      onTap: () {
                        Get.to(
                          ProductDetailsScreen(
                            idProduct:
                                listOffersProducts[index].productId.toString(),
                          ),
                        );
                      },
                    );
                  },
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                ),
              );
            }
          }),
    );
  }
}
