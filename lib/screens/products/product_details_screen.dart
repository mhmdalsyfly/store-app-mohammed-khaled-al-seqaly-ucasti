import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/cart.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/model/product_details.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_basket.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_page_view.dart';
import 'package:store_app/widgets/my_product_item_listview.dart';
import 'package:store_app/widgets/my_rating_bar.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/number_basket.dart';

class ProductDetailsScreen extends StatefulWidget {
  String idProduct;

  ProductDetailsScreen({required this.idProduct});

  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ProductDetails>>(
      future: CategoriesController(context: context)
          .getProductDetails(widget.idProduct),
      builder: (context, snapshot) {
        List<ProductDetails> productDetails = snapshot.data ?? [];
        if (snapshot.hasData) {
          print(snapshot.hasError);
          return Scaffold(
            backgroundColor: AppColors.BG_SLIDER,
            appBar: AppBar(
              backgroundColor: AppColors.BG_SLIDER,
              elevation: 0,
              leading: Container(
                child: MyContainerIconBack(
                  onTap: () {
                    Get.back();
                  },
                ),
                margin: EdgeInsetsDirectional.only(
                    start: SizeConfig.scaleWidth(20)),
              ),
              actions: [
                MyBasket(
                  onTap: () {
                    Get.toNamed('/cart_screen');
                  },
                  child: Container(
                    height: SizeConfig.scaleHeight(45),
                    width: SizeConfig.scaleWidth(45),
                    child: Stack(
                      children: [
                        Center(
                          child: Image.asset(
                            'images/basket.png',
                            height: SizeConfig.scaleHeight(26),
                            width: SizeConfig.scaleWidth(26),
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: double.infinity,
                        ),
                        Obx(() {
                          print(CartControllerGetx.to.listProducts.toString());
                          return NumberBasket();
                        }),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: SizeConfig.scaleWidth(14)),
              ],
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    height: (MediaQuery.of(context).size.height * 0.35),
                    width: double.infinity,
                    color: AppColors.BG_SLIDER,
                    child: MyPageView(
                      listImages: productDetails[0].images,
                      boxFit: BoxFit.contain,
                      color: AppColors.TRANSPARENT,
                      marginBottom: 50,
                      network: true,
                    ),
                  ),
                  Container(
                    padding: EdgeInsetsDirectional.only(
                      start: SizeConfig.scaleHeight(20),
                      end: SizeConfig.scaleHeight(20),
                      top: SizeConfig.scaleHeight(20),
                    ),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: AppColors.WHITE,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: MyText(
                                      title:
                                          SharedPrefController().getLangCode ==
                                                  'en'
                                              ? productDetails[0].nameEn
                                              : productDetails[0].nameAr,
                                      textAlign: TextAlign.start,
                                      fontSize: 20,
                                      color: AppColors.TEXT_Bold,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  SizedBox(height: SizeConfig.scaleHeight(6)),
                                  SizedBox(
                                    width: double.infinity,
                                    child: MyText(
                                      title:
                                          SharedPrefController().getLangCode ==
                                                  'en'
                                              ? productDetails[0].infoEn
                                              : productDetails[0].infoAr,
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible: productDetails[0].offerPrice != null,
                              child: Image.asset(
                                'images/shop_now.png',
                                width: SizeConfig.scaleWidth(50),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(10)),
                        Row(
                          children: [
                            MyText(
                              title: AppLocalizations.of(context)!.price,
                            ),
                            Visibility(
                              visible: productDetails[0].offerPrice != null,
                              child: MyText(
                                title: '${productDetails[0].price} ₪',
                                color: AppColors.GREEN,
                                fontSize: 18,
                                textAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                lineThrough:
                                    productDetails[0].offerPrice != null,
                              ),
                            ),
                            Visibility(
                                visible: productDetails[0].offerPrice != null,
                                child:
                                    SizedBox(width: SizeConfig.scaleWidth(4))),
                            MyText(
                              title:
                                  '${productDetails[0].offerPrice == null ? productDetails[0].price : productDetails[0].offerPrice} ₪',
                              color: AppColors.GREEN,
                              fontSize: 18,
                              textAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                            ),
                            Spacer(),
                            Column(
                              children: [
                                MyText(
                                  title:
                                      productDetails[0].overalRate.toString() +
                                          ' ' +
                                          AppLocalizations.of(context)!
                                              .rating
                                              .toString(),
                                  fontSize: 12,
                                  color: AppColors.TEXT_Bold,
                                ),
                                SizedBox(height: SizeConfig.scaleHeight(8)),
                                MyRatingBar(
                                  ignoreGestures: true,
                                  initialRating: double.parse(
                                      productDetails[0].overalRate.toString()),
                                  function: (rating) {
                                    print(rating);
                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(30)),
                        Row(
                          children: [
                            MyText(
                              title: AppLocalizations.of(context)!
                                  .productRating
                                  .toString(),
                              fontSize: 18,
                              textAlign: TextAlign.start,
                              color: AppColors.TEXT_Bold,
                            ),
                            SizedBox(width: SizeConfig.scaleWidth(26)),
                            MyRatingBar(
                              initialRating: 3.4,
                              ignoreGestures: false,
                              function: (double rating) {
                                CategoriesController(context: context)
                                    .addRate(productDetails[0].id, rating);
                              },
                            ),
                          ],
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(20)),
                        SizedBox(
                          width: double.infinity,
                          child: MyText(
                            title:
                                AppLocalizations.of(context)!.similarProducts,
                            color: AppColors.TEXT_Bold,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            textAlign: TextAlign.start,
                          ),
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(12)),
                        SizedBox(
                          height: SizeConfig.scaleHeight(190),
                          child: FutureBuilder<List<Product>>(
                              future: CategoriesController(context: context)
                                  .getSubCategoryProducts(productDetails[0]
                                      .subCategoryId
                                      .toString()),
                              builder: (context, snapshot) {
                                List<Product> listProducts =
                                    snapshot.data ?? [];
                                if (snapshot.hasData) {
                                  return ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: listProducts.length,
                                    itemBuilder: (context, index) {
                                      return MyProductItemListView(
                                        product: listProducts[index],
                                        height: 190,
                                      );
                                    },
                                  );
                                } else {
                                  return Center(
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          AppColors.BG_BUTTON),
                                    ),
                                  );
                                }
                              }),
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(20)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              color: AppColors.WHITE,
              child: MyContainerShape(
                height: SizeConfig.scaleHeight(92),
                borderRadius: 32,
                width: double.infinity,
                paddingVertical: 20,
                paddingHorizontal: 20,
                bgContainer: AppColors.WHITE,
                shadow: AppColors.BLACK.withOpacity(.1),
                blur: 14,
                xOffset: 0,
                yOffset: -7,
                child: Row(
                  children: [
                    Material(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.WHITE,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(8),
                        onTap: () {
                          addProduct(productDetails[0]);
                        },
                        child: MyContainerShape(
                          enableBorder: true,
                          enableShadow: false,
                          paddingHorizontal: 0,
                          paddingVertical: 0,
                          width: 55,
                          height: SizeConfig.scaleHeight(55),
                          alignment: AlignmentDirectional.center,
                          bgContainer: AppColors.TRANSPARENT,
                          borderRadius: 8,
                          colorBorder: AppColors.BORDER_Container,
                          child: Image.asset(
                            'images/basket.png',
                            height: SizeConfig.scaleHeight(26),
                            width: SizeConfig.scaleWidth(26),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: SizeConfig.scaleWidth(16)),
                    Expanded(
                      child: MyTextButton(
                        title: AppLocalizations.of(context)!.buyNow,
                        colorText: AppColors.WHITE,
                        bgColor: AppColors.BG_BUTTON,
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        height: SizeConfig.scaleHeight(55),
                        enabledSize: true,
                        onPressed: () {
                          addProduct(productDetails[0], cartScreen: true);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          print(snapshot.error.toString());
          return Container(
            width: double.infinity,
            height: double.infinity,
            color: AppColors.WHITE,
            child: Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
              ),
            ),
          );
        }
      },
    );
  }

  bool checkProductFromMyCart(int idProduct) {
    List<Cart> listProductInCart = CartControllerGetx.to.listProducts;
    for (int i = 0; i < listProductInCart.length; i++) {
      if (idProduct == listProductInCart[i].idProduct) {
        return false;
      }
    }
    return true;
  }

  void addProduct(ProductDetails productDetails, {bool cartScreen = false}) {
    if (checkProductFromMyCart(productDetails.id)) {
      if (productDetails.quantity != 0) {
        CartControllerGetx.to.create(Cart(
          idUser: SharedPrefController().getId,
          price: productDetails.offerPrice == null
              ? productDetails.price
              : productDetails.offerPrice!,
          nameEn: productDetails.nameEn,
          nameAr: productDetails.nameAr,
          idProduct: productDetails.id,
          image: productDetails.imageUrl,
          quantity: productDetails.quantity,
          requiredQuantity: 1,
        ));
        Helpers.showSnackBar(context,
            message: AppLocalizations.of(context)!.addedCart);
        if (cartScreen) Get.toNamed('/cart_screen');
      } else {
        Helpers.showSnackBar(
          context,
          message: AppLocalizations.of(context)!.sorryProduct,
          error: true,
        );
      }
    } else {
      if (cartScreen) {
        Get.toNamed('/cart_screen');
        return;
      }
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.productAdded,
        error: true,
      );
    }
  }
}
