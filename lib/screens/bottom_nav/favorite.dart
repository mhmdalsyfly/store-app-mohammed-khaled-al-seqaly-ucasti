import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/screens/products/product_details_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_grid_view_product.dart';
import 'package:store_app/widgets/my_basket_full_icon.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Favorite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.scaleHeight(40)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: AppLocalizations.of(context)!.favorites,
                color: AppColors.TEXT_Bold,
                fontSize: 22,
                fontWeight: FontWeight.w700,
              ),
              MyBasketFullIcon(
                marginEnd: 0,
                onTap: () {
                  Get.toNamed('/cart_screen');
                },
              ),
            ],
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Expanded(
            child: Obx(() {
              if (!CategoriesController.to.isLoading.value) {
                if (CategoriesController.to.listProductFavorite.length > 0) {
                  return GridView.builder(
                    padding: EdgeInsets.zero,
                    itemCount:
                        CategoriesController.to.listProductFavorite.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: SizeConfig.scaleWidth(156) /
                          SizeConfig.scaleHeight(175),
                      crossAxisSpacing: SizeConfig.scaleWidth(14),
                      mainAxisSpacing: SizeConfig.scaleHeight(16),
                    ),
                    itemBuilder: (context, index) {
                      int? offer = CategoriesController
                          .to.listProductFavorite[index].offerPrice;

                      return ItemGridViewProduct(
                        offer: offer == null ? 0 : offer,
                        product:
                            CategoriesController.to.listProductFavorite[index],
                        onTap: () {
                          Get.to(ProductDetailsScreen(
                            idProduct: CategoriesController
                                .to.listProductFavorite[index].id
                                .toString(),
                          ));
                        },
                      );
                    },
                  );
                } else {
                  return Center(
                    child: MyText(
                      title: AppLocalizations.of(context)!.noData,
                    ),
                  );
                }
              } else {
                return Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                  ),
                );
              }
            }),
          ),
        ],
      ),
    );
  }
}
