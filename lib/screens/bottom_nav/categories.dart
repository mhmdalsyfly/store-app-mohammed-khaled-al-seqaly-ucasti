import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/category.dart';
import 'package:store_app/screens/products/category_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_listview_category.dart';
import 'package:store_app/widgets/my_basket_full_icon.dart';
import 'package:store_app/widgets/my_text.dart';

class Categories extends StatelessWidget {
  CategoriesController categoriesController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.scaleHeight(40)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: AppLocalizations.of(context)!.categories,
                color: AppColors.TEXT_Bold,
                fontSize: 22,
                fontWeight: FontWeight.w700,
              ),
              MyBasketFullIcon(
                marginEnd: 0,
                onTap: () {
                  Get.toNamed('/cart_screen');
                },
              ),
            ],
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Expanded(
            child: Obx(() {
              List<Category> listCategory =
                  CategoriesController.to.listCategory;
              if (listCategory.length > 0) {
                return GridView.builder(
                  padding: EdgeInsets.zero,
                  itemCount: listCategory.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: SizeConfig.scaleWidth(154) /
                        SizeConfig.scaleHeight(160),
                    crossAxisSpacing: SizeConfig.scaleWidth(20),
                    mainAxisSpacing: SizeConfig.scaleHeight(20),
                  ),
                  itemBuilder: (context, index) {
                    return ItemListViewCategory(
                      category: listCategory[index],
                      onTap: () {
                        Get.to(CategoryScreen(category: listCategory[index]));
                      },
                      height: 100,
                      width: 100,
                      fontTitle: 18,
                      fontWeight: FontWeight.w500,
                    );
                  },
                );
              } else {
                return Container(
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                  ),
                );
              }
            }),
          ),
        ],
      ),
    );
  }
}
