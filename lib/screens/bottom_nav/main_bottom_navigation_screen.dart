import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/address_controller.dart';
import 'package:store_app/api/controllers_api/card_payment_controller.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/content_bottom_nav.dart';
import 'package:store_app/screens/bottom_nav/profile.dart';
import 'package:store_app/screens/other_controller/controller_username.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_text.dart';
import 'categories.dart';
import 'favorite.dart';
import 'home.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    '1', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

class MainBottomNavigationScreen extends StatefulWidget {
  @override
  _MainBottomNavigationScreenState createState() =>
      _MainBottomNavigationScreenState();
}

class _MainBottomNavigationScreenState
    extends State<MainBottomNavigationScreen> {
  int _currentPage = 0;
  Color _colorItem1 = AppColors.SELECTED_ITEM;
  Color _colorItem2 = AppColors.UN_SELECTED_ITEM;
  Color _colorItem3 = AppColors.UN_SELECTED_ITEM;
  Color _colorItem4 = AppColors.UN_SELECTED_ITEM;
  bool _divider1 = true;
  bool _divider2 = false;
  bool _divider3 = false;
  bool _divider4 = false;

  List<ContentBottomNav> _list = [];

  @override
  void initState() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification!.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                color: AppColors.BG_BUTTON,
                playSound: true,
                icon: '@mipmap/ic_launcher_round.png',
              ),
            ));
      }
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    Get.put(CategoriesController(context: context));
    Get.put(CardPaymentController(context: context));
    Get.put(CartControllerGetx(context: context));
    Get.put(AddressController(context: context));
    Get.put(UsernameController());

    _list = [
      ContentBottomNav(title: AppLocalizations.of(context)!.home, screen: Home()),
      ContentBottomNav(title: AppLocalizations.of(context)!.categories, screen: Categories()),
      ContentBottomNav(title: AppLocalizations.of(context)!.profile, screen: Profile()),
      ContentBottomNav(title: AppLocalizations.of(context)!.favorites, screen: Favorite()),
    ];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      body: _list[_currentPage].screen,
      bottomNavigationBar: Container(
        height: SizeConfig.scaleHeight(70),
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 0),
              color: AppColors.BG_BOTTOM_NAVIGATION,
              blurRadius: 10,
            ),
          ],
        ),
        child: Row(
          children: [
            itemBottomNavigation(
                title: AppLocalizations.of(context)!.home,
                iconData: Icons.dashboard_outlined,
                itemColor: _colorItem1,
                divider: _divider1),
            itemBottomNavigation(
                title: AppLocalizations.of(context)!.favorites,
                iconData: Icons.favorite_border,
                itemColor: _colorItem4,
                divider: _divider4),
            itemBottomNavigation(
                title: AppLocalizations.of(context)!.categories,
                iconData: Icons.category_outlined,
                itemColor: _colorItem2,
                divider: _divider2),
            itemBottomNavigation(
                title: AppLocalizations.of(context)!.profile,
                iconData: Icons.person_rounded,
                itemColor: _colorItem3,
                divider: _divider3),
          ],
        ),
      ),
    );
  }

  Expanded itemBottomNavigation({
    required String title,
    required iconData,
    required Color itemColor,
    required bool divider,
  }) {
    getColorBottomNavigationItem();
    return Expanded(
      child: Container(
        child: Material(
          color: AppColors.WHITE,
          child: InkWell(
            onTap: () {
              choseScreen(title);
              getColorBottomNavigationItem();
              setState(() {});
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  iconData,
                  color: itemColor,
                  size: 22,
                ),
                MyText(
                  title: title,
                  fontSize: 14,
                  color: itemColor,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(height: SizeConfig.scaleHeight(4)),
                Visibility(
                  visible: divider,
                  child: Container(
                    height: SizeConfig.scaleHeight(2),
                    width: SizeConfig.scaleWidth(24),
                    decoration: BoxDecoration(
                      color: AppColors.SELECTED_ITEM,
                      borderRadius: BorderRadius.circular(2),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void getColorBottomNavigationItem() {
    switch (_currentPage) {
      case 0:
        _colorItem1 = AppColors.SELECTED_ITEM;
        _colorItem2 = AppColors.UN_SELECTED_ITEM;
        _colorItem3 = AppColors.UN_SELECTED_ITEM;
        _colorItem4 = AppColors.UN_SELECTED_ITEM;
        _divider1 = true;
        _divider2 = false;
        _divider3 = false;
        _divider4 = false;

        break;
      case 1:
        _colorItem1 = AppColors.UN_SELECTED_ITEM;
        _colorItem2 = AppColors.SELECTED_ITEM;
        _colorItem3 = AppColors.UN_SELECTED_ITEM;
        _colorItem4 = AppColors.UN_SELECTED_ITEM;
        _divider1 = false;
        _divider2 = true;
        _divider3 = false;
        _divider4 = false;
        break;
      case 2:
        _colorItem1 = AppColors.UN_SELECTED_ITEM;
        _colorItem2 = AppColors.UN_SELECTED_ITEM;
        _colorItem3 = AppColors.SELECTED_ITEM;
        _colorItem4 = AppColors.UN_SELECTED_ITEM;
        _divider1 = false;
        _divider2 = false;
        _divider3 = true;
        _divider4 = false;
        break;
      case 3:
        _colorItem1 = AppColors.UN_SELECTED_ITEM;
        _colorItem2 = AppColors.UN_SELECTED_ITEM;
        _colorItem3 = AppColors.UN_SELECTED_ITEM;
        _colorItem4 = AppColors.SELECTED_ITEM;
        _divider1 = false;
        _divider2 = false;
        _divider3 = false;
        _divider4 = true;
        break;
    }
  }

  void choseScreen(String title) {
    if (title == AppLocalizations.of(context)!.home) {
      _currentPage = 0;
    } else if (title == AppLocalizations.of(context)!.categories) {
      _currentPage = 1;
    } else if (title == AppLocalizations.of(context)!.profile) {
      _currentPage = 2;
    } else if (title == AppLocalizations.of(context)!.favorites) {
      _currentPage = 3;
    }
  }

  Widget? trellingIcon() {
    if (_currentPage == 0) {
      return IconButton(
        icon: Icon(
          Icons.settings,
          color: AppColors.BG_BUTTON,
          size: 24,
        ),
        onPressed: () {},
        padding: EdgeInsets.zero,
      );
    } else if (_currentPage == 1) {
      return IconButton(
        icon: Icon(
          Icons.add_circle_sharp,
          color: AppColors.BG_BUTTON,
          size: 24,
        ),
        onPressed: () {},
        padding: EdgeInsets.zero,
      );
    } else {
      return null;
    }
  }
}
