import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/screens/other_controller/controller_username.dart';
import 'package:store_app/screens/products/category_screen.dart';
import 'package:store_app/screens/products/product_details_screen.dart';
import 'package:store_app/screens/products/show_all_product.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_grid_view_product.dart';
import 'package:store_app/widgets/item_listview_category.dart';
import 'package:store_app/widgets/my_basket.dart';
import 'package:store_app/widgets/my_page_view.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/number_basket.dart';

class Home extends StatelessWidget {
  String name = SharedPrefController().getNameUser;

  List<String> _list = [
    'images/banner3.jpg',
    'images/banner1.jpg',
    'images/banner2.jpg',
    'images/banner4.jpg',
    'images/banner5.jpg',
    'images/banner6.jpg',
  ];

  CategoriesController categoriesController = Get.find();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: SizeConfig.scaleHeight(40)),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.scaleWidth(20),
            ),
            child: Row(
              children: [
                Obx(() {
                  return MyText(
                    title: SharedPrefController().getLangCode == 'en'
                        ? 'Hi ,' +
                            UsernameController.to.UserName.value.firstWord()
                        : 'مرحبا  ,' +
                            UsernameController.to.UserName.value.firstWord(),
                    color: AppColors.TEXT_Bold,
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                  );
                }),
                Spacer(),
                MyBasket(
                  onTap: () {
                    Get.toNamed('/cart_screen');
                  },
                  child: Stack(
                    children: [
                      Center(
                        child: Image.asset(
                          'images/basket.png',
                          height: SizeConfig.scaleHeight(26),
                          width: SizeConfig.scaleWidth(26),
                        ),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: double.infinity,
                      ),
                      NumberBasket(),
                    ],
                  ),
                ),
                SizedBox(width: SizeConfig.scaleWidth(14)),
                MyBasket(
                  onTap: () {
                    Get.toNamed('/update_profile_screen');
                  },
                  child: Container(
                    width: SizeConfig.scaleWidth(42),
                    height: SizeConfig.scaleHeight(42),
                    alignment: Alignment.center,
                    child: Obx(() {
                      return MyText(
                        title: UsernameController.to.UserName.value.getCharName(),
                        fontWeight: FontWeight.bold,
                      );
                    }),
                  ),
                ),
              ],
            ),
          ),
          // Padding(
          //   padding: EdgeInsets.symmetric(
          //     horizontal: SizeConfig.scaleWidth(20),
          //   ),
          //   child: SizedBox(
          //     height: SizeConfig.scaleHeight(55),
          //     child: Row(
          //       children: [
          //         Expanded(
          //           child: MySearchView(),
          //         ),
          //         SizedBox(width: SizeConfig.scaleHeight(14)),
          //         MyIconFilter()
          //       ],
          //     ),
          //   ),
          // ),
          SizedBox(
            height: SizeConfig.scaleHeight(190),
            child: MyPageView(
              listImages: _list,
              colorIndicator: AppColors.BG_BUTTON,
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: AppLocalizations.of(context)!.categories,
                  color: AppColors.TEXT_Bold,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  textAlign: TextAlign.start,
                ),
                MyTextButton(
                  title: AppLocalizations.of(context)!.viewAll,
                  bgColor: AppColors.BG_SEE_ALL,
                  colorText: AppColors.TEXT_Bold,
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  onPressed: () {
                    Get.toNamed('/categories_screen');
                  },
                  radius: 2,
                ),
              ],
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(12)),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.scaleWidth(20),
            ),
            child: SizedBox(
              height: SizeConfig.scaleHeight(133),
              child: Obx(() {
                if (categoriesController.listCategory.length > 0) {
                  return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: categoriesController.listCategory.length,
                    itemBuilder: (context, index) {
                      return Row(
                        children: [
                          ItemListViewCategory(
                            category: categoriesController.listCategory[index],
                            onTap: () {
                              Get.to(CategoryScreen(
                                  category: categoriesController
                                      .listCategory[index]));
                            },
                          ),
                          SizedBox(width: SizeConfig.scaleWidth(14)),
                        ],
                      );
                    },
                  );
                } else
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                    ),
                  );
              }),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: AppLocalizations.of(context)!.latestProducts,
                  color: AppColors.TEXT_Bold,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  textAlign: TextAlign.start,
                ),
                MyTextButton(
                  title: AppLocalizations.of(context)!.viewAll,
                  bgColor: AppColors.BG_SEE_ALL,
                  colorText: AppColors.TEXT_Bold,
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  onPressed: () {
                    Get.to(ShowAllProduct(
                        title: AppLocalizations.of(context)!.latestProducts));
                  },
                  radius: 2,
                ),
              ],
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(12)),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.scaleWidth(20),
            ),
            child: FutureBuilder<List<Product>>(
                future: CategoriesController(context: context).getProducts(),
                builder: (context, snapshot) {
                  List<Product> listLastProducts = snapshot.data ?? [];
                  if (listLastProducts.length > 10) {
                    listLastProducts.sublist(0, 9);
                  }
                  if (snapshot.hasData) {
                    return GridView.builder(
                      padding: EdgeInsets.zero,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: listLastProducts.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: SizeConfig.scaleWidth(156) /
                            SizeConfig.scaleHeight(175),
                        crossAxisSpacing: SizeConfig.scaleWidth(14),
                        mainAxisSpacing: SizeConfig.scaleHeight(16),
                      ),
                      itemBuilder: (context, index) {
                        int? offer = listLastProducts[index].offerPrice;
                        return ItemGridViewProduct(
                          offer: offer == null ? 0 : offer,
                          product: listLastProducts[index],
                          onTap: () {
                            Get.to(
                              ProductDetailsScreen(
                                idProduct:
                                    listLastProducts[index].id.toString(),
                              ),
                            );
                          },
                        );
                      },
                    );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                      ),
                    );
                  }
                }),
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: AppLocalizations.of(context)!.productsLike,
                  color: AppColors.TEXT_Bold,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  textAlign: TextAlign.start,
                ),
                MyTextButton(
                  title: AppLocalizations.of(context)!.viewAll,
                  bgColor: AppColors.BG_SEE_ALL,
                  colorText: AppColors.TEXT_Bold,
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  onPressed: () {
                    Get.to(ShowAllProduct(
                        title: AppLocalizations.of(context)!.productsLike));
                  },
                  radius: 2,
                ),
              ],
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(12)),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.scaleWidth(20),
            ),
            child: FutureBuilder<List<Product>>(
              future: CategoriesController(context: context)
                  .getProducts(type: 'famous_products'),
              builder: (context, snapshot) {
                List<Product> listFamousProducts = snapshot.data ?? [];
                if (listFamousProducts.length > 10) {
                  listFamousProducts.sublist(0, 9);
                }
                if (snapshot.hasData) {
                  return GridView.builder(
                    padding: EdgeInsets.zero,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: listFamousProducts.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: SizeConfig.scaleWidth(156) /
                          SizeConfig.scaleHeight(175),
                      crossAxisSpacing: SizeConfig.scaleWidth(14),
                      mainAxisSpacing: SizeConfig.scaleHeight(16),
                    ),
                    itemBuilder: (context, index) {
                      int? offer = listFamousProducts[index].offerPrice;
                      return ItemGridViewProduct(
                        offer: offer == null ? 0 : offer,
                        product: listFamousProducts[index],
                        onTap: () {
                          Get.to(
                            ProductDetailsScreen(
                              idProduct:
                                  listFamousProducts[index].id.toString(),
                            ),
                          );
                        },
                      );
                    },
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                    ),
                  );
                }
              },
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(30)),
        ],
      ),
    );
  }
}

extension ExtentionsString on String {
  String firstWord() {
    String firstName = '';
    for (int i = 0; i < this.length; i++) {
      if (this[i] == ' ') {
        return firstName;
      } else {
        firstName += this[i];
      }
    }
    return '';
  }

  String getCharName() {
    return substring(0, 1).toUpperCase();
  }
}
