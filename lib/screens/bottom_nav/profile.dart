import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/item_profile.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_item_profile.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  List<ItemProfile> listItemProfile = [];

  @override
  void didChangeDependencies() {
    listItemProfile = [
      ItemProfile(
        title: AppLocalizations.of(context)!.editProfile,
        leading: Icons.person,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.changePassword,
        leading: Icons.edit,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.language,
        leading: Icons.language,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.orders,
        leading: Icons.format_list_numbered_sharp,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.paymentCards,
        leading: Icons.payment_outlined,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.addresses,
        leading: Icons.location_on_outlined,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.productsOffers,
        leading: Icons.new_releases_outlined,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.contactUs,
        leading: Icons.contact_mail_outlined,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.about,
        leading: Icons.info_outline,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.privacyPolicy,
        leading: Icons.privacy_tip_outlined,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
      ItemProfile(
        title: AppLocalizations.of(context)!.logout,
        leading: Icons.logout,
        trailing: Icons.arrow_forward_ios_sharp,
      ),
    ];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(10)),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.scaleHeight(40)),
          MyText(
            title: AppLocalizations.of(context)!.profile,
            color: AppColors.TEXT_Bold,
            fontSize: 22,
            fontWeight: FontWeight.w700,
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Expanded(
            child: ListView.separated(
              itemCount: listItemProfile.length,
              padding: EdgeInsets.symmetric(
                vertical: SizeConfig.scaleHeight(20),
              ),
              itemBuilder: (context, index) {
                return MyItemProfile(
                  item: listItemProfile[index],
                  onTap: () {
                    onClickedItem(listItemProfile[index].title, context);
                  },
                );
              },
              separatorBuilder: (context, index) {
                return Divider(
                  height: 0,
                  thickness: 1,
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void onClickedItem(String item, BuildContext context) async {
    if (item == AppLocalizations.of(context)!.language) {
      Get.toNamed('/language_screen');
    } else if (item == AppLocalizations.of(context)!.editProfile) {
      Get.toNamed('/update_profile_screen');
    } else if (item == AppLocalizations.of(context)!.paymentCards) {
      Get.toNamed('/payment_card_screen');
    } else if (item == AppLocalizations.of(context)!.orders) {
      Get.toNamed('/order_screen');
    } else if (item == AppLocalizations.of(context)!.changePassword) {
      Get.toNamed('/change_password_screen');
    } else if (item == AppLocalizations.of(context)!.productsOffers) {
      Get.toNamed('/products_offer_screen');
    } else if (item == AppLocalizations.of(context)!.addresses) {
      Get.toNamed('/address_screen');
    } else if (item == AppLocalizations.of(context)!.contactUs) {
      Get.toNamed('/contact_us_screen');
    } else if (item == AppLocalizations.of(context)!.about) {
      Get.toNamed('/about_screen');
    } else if (item == AppLocalizations.of(context)!.privacyPolicy) {
      Get.toNamed('/privacy_policy_screen');
    } else if (item == AppLocalizations.of(context)!.logout) {
      await SharedPrefController().clearDataFromSharedPre();
      Helpers.showSnackBar(context, message: 'Signed out');
      Get.offAllNamed('/sign_in_screen');
    }
  }
}
