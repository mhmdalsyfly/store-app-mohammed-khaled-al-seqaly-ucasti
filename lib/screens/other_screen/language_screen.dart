import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/container_settings.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LanguageScreen extends StatefulWidget {
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  bool check1 = false;
  bool check2 = false;

  @override
  void didChangeDependencies() {
    if (SharedPrefController().getLangCode == 'en') {
      check2 = true;
    } else {
      check1 = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.language,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleHeight(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(35)),
            SizedBox(
              width: double.infinity,
              child: MyText(
                title:AppLocalizations.of(context)!.chooseLanguage,
                fontWeight: FontWeight.w500,
                fontSize: 15,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.arabic,
              iconDataLeading: Icons.language,
              iconDataTralling: Icons.check,
              visible: check1,
              onPreased: () {
                _checkLanguage('ar');
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.english,
              iconDataLeading: Icons.language,
              iconDataTralling: Icons.check,
              visible: check2,
              onPreased: () {
                _checkLanguage('en');
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(25)),
            MyTextButton(
              onPressed: () async {
                if (check1) {
                  Get.updateLocale(Locale('ar'));
                  await SharedPrefController().setLangCode('ar');
                } else if (check2) {
                  Get.updateLocale(Locale('en'));
                  await SharedPrefController().setLangCode('en');
                }
              },
              title: AppLocalizations.of(context)!.save,
              bgColor: AppColors.BG_BUTTON,
              colorText: AppColors.WHITE,
              radius: 8,
              fontSize: 18,
              fontWeight: FontWeight.normal,
              enabledSize: true,
              height: 55,
              width: double.infinity,
            )
          ],
        ),
      ),
    );
  }

  _checkLanguage(String lang) {
    if (lang == 'en') {
      check1 = false;
      check2 = true;
    } else {
      check1 = true;
      check2 = false;
    }
    setState(() {});
  }
}
