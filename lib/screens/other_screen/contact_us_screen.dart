import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ContactUsScreen extends StatefulWidget {
  const ContactUsScreen({Key? key}) : super(key: key);

  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  bool isLoading = false;

  TextEditingController subjectController = TextEditingController();
  TextEditingController messageController = TextEditingController();

  @override
  void dispose() {
    subjectController.dispose();
    messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      opacity: .2,
      color: AppColors.BLACK,
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
      ),
      child: Scaffold(
        backgroundColor: AppColors.BG_SCREENS_HOME,
        appBar: AppBar(
          title: MyText(
            title: AppLocalizations.of(context)!.contactUs,
            color: AppColors.TEXT_Bold,
            fontSize: 22,
            fontWeight: FontWeight.w700,
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          leading: Padding(
            padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
            child: MyContainerIconBack(
              onTap: () => Get.back(),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(30)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.enterTopic,
                  obscure: false,
                  controller: subjectController,
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.enterContentMessage,
                  controller: messageController,
                  obscure: false,
                  maxMinLine: true,
                ),
                SizedBox(height: SizeConfig.scaleHeight(50)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.callUs,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () {
                    preformSendMessage();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void preformSendMessage() {
    if (checkData()) {
      setState(() {
        isLoading = true;
      });
      sendMessage();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool checkData() {
    if (subjectController.text.isNotEmpty &&
        messageController.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  void sendMessage() async {
    bool changed = await AuthController().sendMessageToAdmin(context, {
      'subject': subjectController.text,
      'message': messageController.text,
    });
    if (changed) {
      setState(() {
        isLoading = false;
      });
      Get.back();
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }
}

