import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/icon_social_media.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.about,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: double.infinity,
            ),
            MyContainerShape(
              width: 230,
              height: SizeConfig.scaleHeight(140),
              enableBorder: false,
              enableShadow: false,
              enableRadius: true,
              alignment: AlignmentDirectional.center,
              paddingHorizontal: 20,
              paddingVertical: 10,
              borderRadius: 12,
              bgContainer: AppColors.WHITE,
              child: Image.asset('images/logo2.png'),
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
            MyText(
              title: SharedPrefController().getLangCode == 'en'
                  ? 'Mohammed Khaled Al-Seqaly'
                  : 'محمد خالد السيقلي',
              color: AppColors.TEXT_Bold,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(height: SizeConfig.scaleHeight(4)),
            MyText(
              title: SharedPrefController().getLangCode == 'en'
                  ? 'PallLancer' : 'بال لانسر',
              color: AppColors.BG_BUTTON,
              fontSize: 18,
            ),
            SizedBox(height: SizeConfig.scaleHeight(20)),
            Row(
              children: [
                IconSocialMedia(
                  bgColor: AppColors.BG_ICON_FACEBOOK,
                  icon: 'images/facebook.png',
                  onTap: () {
                    _launchURL('https://www.facebook.com/Mhammedkhaled0');
                  },
                ),
                SizedBox(width: SizeConfig.scaleWidth(25)),
                IconSocialMedia(
                  icon: 'images/linkedin.png',
                  bgColor: AppColors.BG_ICON_LINKEDIN,
                  onTap: () {
                    _launchURL('https://www.linkedin.com/in/mhammedkhaled0/');
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _launchURL(String _url) async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';
}
