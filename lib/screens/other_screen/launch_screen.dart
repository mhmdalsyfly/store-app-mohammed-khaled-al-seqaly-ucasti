import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/size_config.dart';

class LaunchScreen extends StatefulWidget {
  const LaunchScreen({Key? key}) : super(key: key);

  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void didChangeDependencies() {
    SizeConfig().init(context);
    super.didChangeDependencies();
  }

  @override
  void initState() {
    Future.delayed(Duration(seconds: 2),(){
      if(SharedPrefController().getMobileUser == ''){
        Get.offNamed('/page_view_screen');
      }else {
        Get.offNamed('/main_bottom_navigation_screen');
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'images/bg_launch.png',
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          Center(
            child: Image.asset(
              'images/logo.png',
              height: SizeConfig.scaleHeight(106),
              width: SizeConfig.scaleWidth(252),
            ),
          )
        ],
      ),
    );
  }
}
