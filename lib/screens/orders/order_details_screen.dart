import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/model/order_product.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_order_details.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderDetailsScreen extends StatelessWidget with MixinSittingsApp {
  int idOrder;
  List<OrderProduct> listProduct = [];

  OrderDetailsScreen({this.idOrder = 1});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.orderDetailProducts,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
          vertical: SizeConfig.scaleHeight(20),
        ),
        child: FutureBuilder<Map<String, dynamic>>(
            future:
                CartControllerGetx(context: context).getDetailsOrder(idOrder),
            builder: (context, snapshot) {
              Map<String, dynamic> objectMap = snapshot.data ?? {};
              if (snapshot.hasData) {
                listProduct = getListFromMap(objectMap['products']);
                return Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  MyText(
                                    title: AppLocalizations.of(context)!
                                        .numberOrder,
                                    textAlign: TextAlign.start,
                                    fontSize: 14,
                                  ),
                                  SizedBox(width: SizeConfig.scaleWidth(8)),
                                  MyText(
                                    title:
                                        objectMap['products_count'].toString(),
                                    color: AppColors.TEXT_Bold,
                                  ),
                                ],
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(8)),
                              Row(
                                children: [
                                  MyText(
                                    title: AppLocalizations.of(context)!.total,
                                    textAlign: TextAlign.start,
                                    fontSize: 14,
                                    height: 1,
                                  ),
                                  SizedBox(width: SizeConfig.scaleWidth(8)),
                                  MyText(
                                    title: objectMap['total'].toString(),
                                    color: AppColors.BG_BUTTON,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ],
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(8)),
                              Row(
                                children: [
                                  MyText(
                                    title: AppLocalizations.of(context)!.date,
                                    textAlign: TextAlign.start,
                                    fontSize: 14,
                                  ),
                                  SizedBox(width: SizeConfig.scaleWidth(8)),
                                  MyText(
                                    title: DateFormat('yyyy-MM-dd').format(
                                      DateTime.parse(objectMap['date']),
                                    ),
                                    color: AppColors.TEXT_Bold,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  MyText(
                                    title: AppLocalizations.of(context)!
                                        .typePayment,
                                    textAlign: TextAlign.start,
                                    fontSize: 14,
                                    height: 1,
                                  ),
                                  SizedBox(width: SizeConfig.scaleWidth(8)),
                                  MyText(
                                    title: objectMap['payment_type'],
                                    color: AppColors.TEXT_Bold,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ],
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(8)),
                              Row(
                                children: [
                                  MyText(
                                    title:
                                        AppLocalizations.of(context)!.address,
                                    textAlign: TextAlign.start,
                                    fontSize: 14,
                                  ),
                                  SizedBox(width: SizeConfig.scaleWidth(8)),
                                  MyText(
                                    title: SharedPrefController().getLangCode ==
                                            'en'
                                        ? objectMap['name_en_city']
                                        : objectMap['name_ar_city'] +
                                            ' ' +
                                            objectMap['name_address'],
                                    color: AppColors.TEXT_Bold,
                                  ),
                                ],
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(8)),
                              Row(
                                children: [
                                  MyText(
                                    title: AppLocalizations.of(context)!.statusOrder,
                                    textAlign: TextAlign.start,
                                    fontSize: 14,
                                  ),
                                  SizedBox(width: SizeConfig.scaleWidth(8)),
                                  MyText(
                                    title: objectMap['status'],
                                    color: AppColors.BG_BUTTON,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(20)),
                    Expanded(
                      child: ListView.builder(
                        itemCount: listProduct.length,
                        itemBuilder: (ctx, index) {
                          return Column(
                            children: [
                              ItemOrderDetails(
                                  orderProduct: listProduct[index]),
                              SizedBox(
                                height: SizeConfig.scaleHeight(20),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                  ),
                );
              }
            }),
      ),
    );
  }

  List<OrderProduct> getListFromMap(objectMap) {
    List<OrderProduct> listProduct = [];
    if (objectMap != null)
      objectMap.forEach((v) {
        listProduct.add(OrderProduct.fromJson(v));
      });
    return listProduct;
  }
}
