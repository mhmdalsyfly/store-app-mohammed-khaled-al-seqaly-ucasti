import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/model/get_order.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/order_item.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.orders,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
          vertical: SizeConfig.scaleHeight(20),
        ),
        child: FutureBuilder<List<GetOrder>>(
          future: CartControllerGetx(context: context).getAllOrders(),
          builder: (ctx, snapshot) {
            List<GetOrder> listOrder = snapshot.data ?? [];
            if (snapshot.hasData) {
              if (listOrder.length > 0) {
                return ListView.builder(
                  itemCount: listOrder.length,
                  itemBuilder: (ctx, index) {
                    return Column(
                      children: [
                        OrderItem(order: listOrder[index]),
                        SizedBox(height: SizeConfig.scaleHeight(20)),
                      ],
                    );
                  },
                );
              } else {
                return Center(
                  child: Text(AppLocalizations.of(context)!.noData),
                );
              }
            } else {
              return Center(
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
