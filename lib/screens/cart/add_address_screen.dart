import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/address_controller.dart';
import 'package:store_app/api/controllers_api/city_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/address.dart';
import 'package:store_app/model/city.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_TextField2.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddAddressScreen extends StatefulWidget {
  Address? address;
  int? index;

  AddAddressScreen({
    this.address,
    this.index,
  });

  @override
  _AddAddressScreenState createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  CityController cityController = Get.put(CityController());

  City? citySelected;
  bool citySelected1 = true;
  bool isLoading = false;

  late TextEditingController nameController;
  late TextEditingController infoController;
  late TextEditingController contactNumController;

  @override
  void initState() {
    nameController = TextEditingController();
    infoController = TextEditingController();
    contactNumController = TextEditingController();
    if (widget.address != null) {
      nameController.text = widget.address!.name;
      infoController.text = widget.address!.info;
      contactNumController.text = widget.address!.contactNumber.toString();
    }
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    infoController.dispose();
    contactNumController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: widget.address == null
              ? AppLocalizations.of(context)!.addAddress
              : AppLocalizations.of(context)!.editAddress,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        opacity: .2,
        color: AppColors.BLACK,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(20),
            vertical: SizeConfig.scaleHeight(10),
          ),
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.name,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyTextField(
                      textInputType: TextInputType.text,
                      hint: AppLocalizations.of(context)!.name,
                      controller: nameController,
                      borderColor: AppColors.BORDER_TEXT_FIELD,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.addressDetails,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyTextField(
                      textInputType: TextInputType.text,
                      controller: infoController,
                      borderColor: AppColors.BORDER_TEXT_FIELD,
                      hint: AppLocalizations.of(context)!.addressDetails,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.contactNumber,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyTextField(
                      textInputType: TextInputType.number,
                      controller: contactNumController,
                      borderColor: AppColors.BORDER_TEXT_FIELD,
                      hint: AppLocalizations.of(context)!.contactNumber,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.city,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyContainerShape(
                      height: SizeConfig.scaleHeight(60),
                      width: double.infinity,
                      paddingHorizontal: 14,
                      enableRadius: true,
                      borderRadius: 8,
                      enableShadow: false,
                      enableBorder: true,
                      colorBorder: AppColors.LABEL_HINT.withOpacity(.2),
                      paddingVertical: 0,
                      alignment: AlignmentDirectional.center,
                      bgContainer: AppColors.TRANSPARENT,
                      child: dropDownList(),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(40)),
                    MyTextButton(
                      onPressed: () {
                        preformSaveAddress(widget.index ?? 0);
                      },
                      title: widget.address == null
                          ? AppLocalizations.of(context)!.save
                          : AppLocalizations.of(context)!.edit,
                      bgColor: AppColors.BG_BUTTON,
                      colorText: AppColors.WHITE,
                      radius: 8,
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      enabledSize: true,
                      height: 55,
                      width: double.infinity,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dropDownList() {
    return Obx(
      () {
        if (cityController.cities.isNotEmpty) {
          if (citySelected1) {
            if (widget.address != null) {
              for (int i = 0; i < cityController.cities.length; i++) {
                if (widget.address!.city!.id == cityController.cities[i].id) {
                  citySelected = cityController.cities[i];
                }
              }
            } else {
              citySelected = cityController.cities.first;
            }
            citySelected1 = false;
          }
          return DropdownButtonFormField<City>(
            isDense: true,
            value: citySelected,
            decoration: InputDecoration(
              border: InputBorder.none,
            ),
            onChanged: (City? city) {
              citySelected = city;
            },
            items: cityController.cities.map((City value) {
              return DropdownMenuItem<City>(
                value: value,
                child: MyText(
                    title: SharedPrefController().getLangCode == 'en'
                        ? value.nameEn
                        : value.nameAr,
                    fontSize: 16),
              );
            }).toList(),
          );
        } else {
          return Align(
            alignment: AlignmentDirectional.centerEnd,
            child: SizedBox(
              height: SizeConfig.scaleHeight(14),
              width: SizeConfig.scaleWidth(14),
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
              ),
            ),
          );
        }
      },
    );
  }

  Address get address => Address(
        id: widget.address == null ? 0 : widget.address!.id,
        name: nameController.text,
        info: infoController.text,
        contactNumber: int.parse(contactNumController.text),
        cityId: citySelected!.id,
        city: citySelected,
      );

  void preformSaveAddress(int index) async {
    if (checkData()) {
      setState(() {
        isLoading = true;
      });
      bool processCheck;
      if (widget.address == null) {
        processCheck = await AddressController.to.addAddress(context, address);
      } else {
        processCheck =
            await AddressController.to.editAddress(context, address, index);
      }
      if (processCheck) {
        setState(() {
          isLoading = false;
        });
        Get.back();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool checkData() {
    if (nameController.text.isNotEmpty &&
        infoController.text.isNotEmpty &&
        contactNumController.text.isNotEmpty &&
        citySelected != null) {
      return true;
    }

    return false;
  }
}
