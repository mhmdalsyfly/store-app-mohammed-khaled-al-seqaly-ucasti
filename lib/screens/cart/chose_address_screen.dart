import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/address_controller.dart';
import 'package:store_app/model/address.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/chose_item_address.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChoseAddressScreen extends StatefulWidget {
  @override
  _ChoseAddressScreenState createState() => _ChoseAddressScreenState();
}

class _ChoseAddressScreenState extends State<ChoseAddressScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.chooseAddress,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
          vertical: SizeConfig.scaleHeight(20),
        ),
        child: Obx(() {
          List<Address> listAddress = AddressController.to.listAddress;
          if (listAddress.length > 0) {
            return ListView.builder(
              itemCount: listAddress.length,
              itemBuilder: (ctx, index) {
                return ItemAddress2(
                  address: listAddress[index],
                  onTap: () {
                    Get.back(result: listAddress[index]);
                  },
                );
              },
            );
          } else {
            return Container(
              width: double.infinity,
              height: double.infinity,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                ),
              ),
            );
          }
        }),
      ),
    );
  }
}
