import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/card_payment_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_TextField2.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddPaymentCardScreen extends StatefulWidget {
  String title;
  CardPayment? cardPayment;
  int? index;

  AddPaymentCardScreen({
    this.title = 'Add Card Payment',
    this.cardPayment,
    this.index,
  });

  @override
  _AddPaymentCardScreenState createState() => _AddPaymentCardScreenState();
}

class _AddPaymentCardScreenState extends State<AddPaymentCardScreen> {
  late TextEditingController holderName;
  late TextEditingController cardNumber;
  late TextEditingController cvv;

  String _selectedItem = 'Visa';
  List<String> deviceTypes = ["Visa", "Master"];

  DateTime selectedDate = DateTime.now();
  String date = "Chose Date";
  bool isLoading = false;

  @override
  void initState() {
    holderName = TextEditingController();
    cardNumber = TextEditingController();
    cvv = TextEditingController();
    if (widget.cardPayment != null) {
      holderName.text = widget.cardPayment!.holderName;
      cardNumber.text = widget.cardPayment!.cardNumber;
      cvv.text = widget.cardPayment!.cvv.toString();
      date = widget.cardPayment!.expDate;
      if (widget.cardPayment!.type.toLowerCase() == 'visa') {
        _selectedItem = 'Visa';
      } else {
        _selectedItem = 'Master';
      }
    }
    super.initState();
  }

  @override
  dispose() {
    holderName.dispose();
    cardNumber.dispose();
    cvv.dispose();
    super.dispose();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
      date = "${selectedDate.toLocal()}".split(' ')[0];
      print(date);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.title == 'Add Card Payment') {
      widget.title = AppLocalizations.of(context)!.addCardPayment;
    } else {
      widget.title = AppLocalizations.of(context)!.editCardPayment;
    }
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: widget.title,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        opacity: .2,
        color: AppColors.BLACK,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
        ),
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(20),
            vertical: SizeConfig.scaleHeight(10),
          ),
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.nameHolder,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyTextField(
                      textInputType: TextInputType.text,
                      hint: AppLocalizations.of(context)!.nameHolder,
                      controller: holderName,
                      borderColor: AppColors.LABEL_HINT.withOpacity(.2),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.cardNumber,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyTextField(
                      textInputType: TextInputType.number,
                      borderColor: AppColors.LABEL_HINT.withOpacity(.2),
                      hint: AppLocalizations.of(context)!.cardNumber,
                      controller: cardNumber,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.cvvNum,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyTextField(
                      textInputType: TextInputType.number,
                      borderColor: AppColors.LABEL_HINT.withOpacity(.2),
                      hint: AppLocalizations.of(context)!.cvvNum,
                      controller: cvv,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.typeCard,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    MyContainerShape(
                      height: SizeConfig.scaleHeight(60),
                      width: double.infinity,
                      paddingHorizontal: 14,
                      enableRadius: true,
                      borderRadius: 8,
                      enableShadow: false,
                      enableBorder: true,
                      colorBorder: AppColors.LABEL_HINT.withOpacity(.2),
                      paddingVertical: 0,
                      alignment: AlignmentDirectional.center,
                      bgContainer: AppColors.TRANSPARENT,
                      child: dropDownList(),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(10)),
                    SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: AppLocalizations.of(context)!.expirationDate,
                        color: AppColors.LABEL_HINT,
                        fontSize: 14,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(7)),
                    GestureDetector(
                      onTap: () {
                        _selectDate(context);
                      },
                      child: MyContainerShape(
                        height: SizeConfig.scaleHeight(60),
                        width: double.infinity,
                        paddingHorizontal: 14,
                        enableRadius: true,
                        borderRadius: 8,
                        enableShadow: false,
                        enableBorder: true,
                        colorBorder: AppColors.LABEL_HINT.withOpacity(.2),
                        paddingVertical: 0,
                        alignment: AlignmentDirectional.center,
                        bgContainer: AppColors.TRANSPARENT,
                        child: MyText(
                          title: date,
                        ),
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(40)),
                    MyTextButton(
                      onPressed: () {
                        preformSave();
                      },
                      title: AppLocalizations.of(context)!.save,
                      bgColor: AppColors.BG_BUTTON,
                      colorText: AppColors.WHITE,
                      radius: 8,
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      enabledSize: true,
                      height: 55,
                      width: double.infinity,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget dropDownList() {
    return DropdownButtonFormField<String>(
      value: _selectedItem,
      isDense: true,
      decoration: InputDecoration(
        border: InputBorder.none,
      ),
      onChanged: (newValue) {
        setState(() {
          _selectedItem = newValue!;
        });
        print(_selectedItem);
      },
      items: deviceTypes.map((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: MyText(title: value, fontSize: 16),
        );
      }).toList(),
    );
  }

  void preformSave() async {
    if (checkData()) {
      setState(() {
        isLoading = true;
      });

      bool processCheck;
      if (widget.cardPayment == null) {
        processCheck =
        await CardPaymentController.to.addPayment(context, cardPayment);
      } else {
        processCheck = await CardPaymentController.to
            .editAddress(context, cardPayment, widget.index!);
      }
      if (processCheck) {
        setState(() {
          isLoading = false;
        });
        Get.back();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    } else {
      Helpers.showSnackBar(context,
          message: AppLocalizations.of(context)!.checkFields, error: true);
    }
  }

  bool checkData() {
    if (holderName.text.isNotEmpty &&
        cardNumber.text.isNotEmpty &&
        cvv.text.isNotEmpty &&
        date.toLowerCase() != 'chose date') {
      return true;
    }
    return false;
  }

  CardPayment get cardPayment =>
      CardPayment(
        holderName: holderName.text,
        cardNumber: cardNumber.text,
        expDate: date,
        cvv: int.parse(cvv.text),
        type: _selectedItem,
        userId: 0,
        updatedAt: '',
        createdAt: '',
        id: widget.cardPayment == null ? 0 : widget.cardPayment!.id,
      );
}
