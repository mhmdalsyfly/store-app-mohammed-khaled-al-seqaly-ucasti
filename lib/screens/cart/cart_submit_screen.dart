import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/card_payment_controller.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/address.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/model/order.dart';
import 'package:store_app/model/radio_model.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_page_view2.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CartSubmitScreen extends StatefulWidget {
  @override
  _CartSubmitScreenState createState() => _CartSubmitScreenState();
}

class _CartSubmitScreenState extends State<CartSubmitScreen> {
  bool radioItem1 = false;
  bool radioItem2 = false;

  bool isCach = false;
  bool isLoading = false;

  Address? address;
  CardPayment? _cardPayment;
  String? cashOrOnline;
  String chooseCart = '';
  String chooseAddress = '';

  @override
  void didChangeDependencies() {
    chooseCart = AppLocalizations.of(context)!.chooseCart;
    chooseAddress = AppLocalizations.of(context)!.chooseAddress;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.cartSubmit,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: LoadingOverlay(
        isLoading: isLoading,
        opacity: .2,
        color: AppColors.BLACK,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
        ),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(20)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(20),
              ),
              child: radioItem(
                  RadioModel(AppLocalizations.of(context)!.cash, radioItem1)),
            ),
            SizedBox(height: SizeConfig.scaleHeight(16)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(20),
              ),
              child: radioItem(
                  RadioModel(AppLocalizations.of(context)!.online, radioItem2)),
            ),
            SizedBox(height: SizeConfig.scaleHeight(16)),
            Visibility(
              visible: isCach,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(20),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      child: MyText(
                        title: chooseCart,
                        color: AppColors.TEXT_Bold,
                        fontSize: 20,
                        textAlign: TextAlign.start,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(220),
                    child: Obx(() {
                      List<CardPayment> listCardPayment =
                          CardPaymentController.to.listCards;
                      if (listCardPayment.length > 0) {
                        return MyPageView2(
                          listCardPayment: CardPaymentController.to.listCards,
                          colorIndicator: AppColors.BG_BUTTON,
                          function: (CardPayment cardPayment) {
                            _cardPayment = cardPayment;
                            print(cardPayment.holderName);
                          },
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                AppColors.BG_BUTTON),
                          ),
                        );
                      }
                    }),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(20)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(20),
              ),
              child: Material(
                color: AppColors.TRANSPARENT,
                borderRadius: BorderRadius.circular(8),
                child: InkWell(
                  onTap: () async {
                    Address data = await Get.toNamed('/chose_address_screen');
                    address = data;
                    if (address != null) {
                      chooseAddress = address!.name +
                          '  -  ' +
                          '${address!.city == null ? '' : SharedPrefController().getLangCode == 'en' ? address!.city!.nameEn : address!.city!.nameAr}';
                      setState(() {});
                    }
                  },
                  borderRadius: BorderRadius.circular(8),
                  child: MyContainerShape(
                    height: SizeConfig.scaleHeight(60),
                    width: double.infinity,
                    paddingHorizontal: 14,
                    enableRadius: true,
                    borderRadius: 8,
                    bgContainer: AppColors.TRANSPARENT,
                    enableShadow: false,
                    enableBorder: true,
                    colorBorder: AppColors.LABEL_HINT.withOpacity(.2),
                    paddingVertical: 0,
                    alignment: AlignmentDirectional.center,
                    child: MyText(
                      title: chooseAddress,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(50)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(20),
              ),
              child: MyTextButton(
                onPressed: () {
                  sendCart();
                },
                title: AppLocalizations.of(context)!.save,
                bgColor: AppColors.BG_BUTTON,
                colorText: AppColors.WHITE,
                radius: 8,
                fontSize: 18,
                fontWeight: FontWeight.normal,
                enabledSize: true,
                height: 55,
                width: double.infinity,
              ),
            )
          ],
        ),
      ),
    );
  }

  radioItem(RadioModel _item) {
    return GestureDetector(
      onTap: () {
        setState(() {
          getColorRadioItem(_item.title);
        });
      },
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              margin: EdgeInsetsDirectional.only(end: 10.0),
              child: MyText(
                title: _item.title,
                color: AppColors.TEXT_Bold,
                fontWeight: FontWeight.w500,
              ),
            ),
            Container(
              height: SizeConfig.scaleHeight(25),
              width: SizeConfig.scaleWidth(25),
              child: Center(
                child: Visibility(
                  visible: _item.selected,
                  child: Icon(
                    Icons.check,
                    color: AppColors.WHITE,
                    size: 18,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color:
                    _item.selected ? AppColors.BG_BUTTON : Colors.transparent,
                border: Border.all(
                  width: 1.0,
                  color: _item.selected ? AppColors.BG_BUTTON : Colors.grey,
                ),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(
                    6.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getColorRadioItem(String title) {
    if (title == AppLocalizations.of(context)!.cash) {
      radioItem1 = true;
      radioItem2 = false;
      isCach = false;
      cashOrOnline = 'Cash';
    } else {
      radioItem1 = false;
      radioItem2 = true;
      isCach = true;
      cashOrOnline = 'Online';
    }
  }

  void sendCart() async {
    if (chekData()) {
      setState(() {
        isLoading = true;
      });
      bool send =
          await CartControllerGetx(context: context).sendOrder(context, order);
      if (send) {
        CartControllerGetx.to.clearData();
        Get.toNamed('/order_screen');
        setState(() {
          isLoading = false;
        });
      } else {
        setState(() {
          isLoading = false;
        });
      }
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool chekData() {
    if (cashOrOnline != null && address != null) {
      return true;
    }
    return false;
  }

  Order get order => Order(
      paymentType: cashOrOnline!,
      addressId: address!.id,
      cardId: _cardPayment == null ? 0 : _cardPayment!.id,
      cart: CartControllerGetx.to.listCart);
}
