import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/card_payment_controller.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_card_payment.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_floatingButton.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PaymentCardsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.onlinePaymentCards,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
          vertical: SizeConfig.scaleHeight(16),
        ),
        child: Obx(() {
          if (!CardPaymentController.to.isLoading.value) {
            List<CardPayment> listCardPayment =
                CardPaymentController.to.listCards;
            if (listCardPayment.length > 0) {
              return ListView.builder(
                itemCount: listCardPayment.length,
                itemBuilder: (ctx, index) {
                  return Column(
                    children: [
                      ItemCardPayment(
                          cardPayment: listCardPayment[index], index: index),
                      SizedBox(
                        height: SizeConfig.scaleHeight(20),
                      ),
                    ],
                  );
                },
              );
            } else {
              return Container(
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.center,
                child: MyText(
                  title: AppLocalizations.of(context)!.noData,
                ),
              );
            }
          }else {
            return Container(
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.center,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
              ),
            );
          }
        }),
      ),
      floatingActionButton: MyFloatingButton(
        iconData: Icons.add,
        route: '/add_payment_card_screen',
        bgColor: AppColors.BG_BUTTON,
      ),
    );
  }
}
