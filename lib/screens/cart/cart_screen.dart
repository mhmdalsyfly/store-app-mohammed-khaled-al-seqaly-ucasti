import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/model/cart.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/item_list_view_my_cart.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREENS_HOME,
      appBar: AppBar(
        title: MyText(
          title: AppLocalizations.of(context)!.myCart,
          color: AppColors.TEXT_Bold,
          fontSize: 22,
          fontWeight: FontWeight.w700,
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: Padding(
          padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
          child: MyContainerIconBack(
            onTap: () => Get.back(),
          ),
        ),
        actions: [
          IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(
              Icons.delete_sweep_outlined,
              color: AppColors.RED,
            ),
            onPressed: () {
              CartControllerGetx.to.clearData();
            },
          ),
        ],
      ),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
          vertical: SizeConfig.scaleHeight(16),
        ),
        child: Column(
          children: [
            Expanded(
              child: Obx(() {
                List<Cart> listProduct = CartControllerGetx.to.listProducts;
                if (!CartControllerGetx.to.isLoading.value) {
                  if (listProduct.length > 0) {
                    return ListView.builder(
                      itemCount: listProduct.length,
                      itemBuilder: (ctx, index) {
                        return Column(
                          children: [
                            MyContainerShape(
                              paddingVertical: 10,
                              paddingHorizontal: 8,
                              width: double.infinity,
                              borderRadius: 8,
                              enableShadow: false,
                              height: SizeConfig.scaleHeight(150),
                              bgContainer: AppColors.WHITE,
                              child: ItemListViewMyCart(index: index),
                            ),
                            SizedBox(
                              height: SizeConfig.scaleHeight(20),
                            ),
                          ],
                        );
                      },
                    );
                  } else {
                    return Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: Center(
                        child: MyText(
                          title: AppLocalizations.of(context)!.noData,
                          fontSize: 16,
                        ),
                      ),
                    );
                  }
                } else {
                  return Container(
                    width: double.infinity,
                    height: double.infinity,
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                      ),
                    ),
                  );
                }
              }),
            ),
            SizedBox(height: SizeConfig.scaleHeight(20)),
            Obx(() {
              return Visibility(
                  visible: CartControllerGetx.to.listProducts.length > 0,
                  child: MyTextButton(
                    title: AppLocalizations.of(context)!.cartSubmit,
                    bgColor: AppColors.BG_BUTTON,
                    colorText: AppColors.WHITE,
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                    height: 55,
                    width: double.infinity,
                    radius: 8,
                    enabledSize: true,
                    onPressed: () {
                      Get.toNamed('/cart_submit_screen');
                      print(CartControllerGetx.to.listCart[0].quantity);
                    },
                  ));
            }),
          ],
        ),
      ),
    );
  }
}
