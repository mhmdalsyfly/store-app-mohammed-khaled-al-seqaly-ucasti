import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/screens/auth/reset_password_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class VerificationScreen extends StatefulWidget {
  String? title;
  String? mobile;
  String? code;

  VerificationScreen({this.title, this.mobile, this.code});

  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  AuthController controllerAuth = Get.put(AuthController());

  Color bg = AppColors.WHITE;
  String pinCode = '';

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    pinCode = widget.code == null ? '0' : widget.code!;
    AuthController.to.pin1.value = pinCode.substring(0, 1);
    AuthController.to.pin2.value = pinCode.substring(1, 2);
    AuthController.to.pin3.value = pinCode.substring(2, 3);
    AuthController.to.pin4.value = pinCode.substring(3, 4);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREEN,
      body: LoadingOverlay(
        isLoading: isLoading,
        opacity: .2,
        color: AppColors.BLACK,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(58)),
                MyContainerIconBack(
                  onTap: () {
                    Get.back();
                  },
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(20),
                ),
                MyText(
                  title: AppLocalizations.of(context)!.verificationCode,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: AppColors.TEXT_Bold,
                ),
                SizedBox(height: SizeConfig.scaleHeight(7)),
                MyText(
                  title: AppLocalizations.of(context)!.enterPin,
                  color: AppColors.FIRST_TEXT_LOGIN,
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
                SizedBox(height: SizeConfig.scaleHeight(20)),
                SizedBox(
                  height: SizeConfig.scaleHeight(55),
                  child: Obx(() {
                    print(AuthController.to.pin1);
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        boxPinCode(AuthController.to.pin1.value),
                        SizedBox(width: SizeConfig.scaleWidth(12)),
                        boxPinCode(AuthController.to.pin2.value),
                        SizedBox(width: SizeConfig.scaleWidth(12)),
                        boxPinCode(AuthController.to.pin3.value),
                        SizedBox(width: SizeConfig.scaleWidth(12)),
                        boxPinCode(AuthController.to.pin4.value),
                      ],
                    );
                  }),
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                allNumbers(),
                SizedBox(height: SizeConfig.scaleHeight(20)),
                // MyText(
                //   title: 'Please enter the code sent',
                //   textAlign: TextAlign.start,
                // ),
                // SizedBox(height: SizeConfig.scaleHeight(8)),
                // CustomTimer(
                //   from: Duration(minutes: 10),
                //   to: Duration(minutes: 0),
                //   onBuildAction: CustomTimerAction.auto_start,
                //   builder: (CustomTimerRemainingTime remaining) {
                //     return Row(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       children: [
                //         boxPinCode("${remaining.minutes}"),
                //         marginWidth(width: 4),
                //         MyText(
                //           title: ':',
                //           color: AppColors.TEXT_Bold,
                //           fontSize: 28,
                //           fontWeight: FontWeight.w900,
                //         ),
                //         marginWidth(width: 4),
                //         boxPinCode("${remaining.seconds}"),
                //       ],
                //     );
                //   },
                // ),
                SizedBox(height: SizeConfig.scaleHeight(20)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.codeVerification,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () async {
                    codeSendingProcess();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool preformVerification() {
    if (pinCode.length == 4) {
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: AppLocalizations.of(context)!.checkFields,
      error: true,
    );
    return false;
  }

  Color getColor(String text) {
    if (text.isNotEmpty) return AppColors.BG_BUTTON;
    return AppColors.WHITE;
  }

  Widget boxNum(int num, {bool check = true, bool delete = false}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (!delete) {
            if (pinCode.length < 4) {
              pinCode += num.toString();
              fillPinCode();
            }
          } else {
            if (pinCode.length > 0)
              pinCode = pinCode.substring(0, pinCode.length - 1);
            deletePinCode();
          }
        });
      },
      child: MyContainerShape(
        borderRadius: 19,
        xOffset: 0,
        yOffset: 10,
        blur: 18,
        shadow: AppColors.BLACK.withOpacity(.2),
        paddingHorizontal: 0,
        paddingVertical: 0,
        alignment: AlignmentDirectional.center,
        width: 60,
        height: SizeConfig.scaleHeight(60),
        child: check
            ? MyText(
                title: '$num',
                color: AppColors.BG_BUTTON,
                fontSize: 23,
              )
            : Icon(
                Icons.arrow_back,
                color: AppColors.BG_BUTTON,
                size: 24,
              ),
      ),
    );
  }

  Column allNumbers() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(1),
            marginWidth(),
            boxNum(2),
            marginWidth(),
            boxNum(3),
          ],
        ),
        marginHeight(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(4),
            marginWidth(),
            boxNum(5),
            marginWidth(),
            boxNum(6),
          ],
        ),
        marginHeight(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(7),
            marginWidth(),
            boxNum(8),
            marginWidth(),
            boxNum(9),
          ],
        ),
        marginHeight(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(100, check: false, delete: true),
            marginWidth(),
            boxNum(0),
            marginWidth(),
            emptyContainer(),
          ],
        ),
      ],
    );
  }

  SizedBox marginWidth({double width = 32}) {
    return SizedBox(width: SizeConfig.scaleWidth(width));
  }

  Container boxPinCode(String text) {
    return Container(
      height: SizeConfig.scaleHeight(40),
      width: SizeConfig.scaleWidth(40),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: getColor(text),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: AppColors.BLACK.withOpacity(.16),
            blurRadius: 6,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: MyText(
        title: text,
        color: AppColors.WHITE,
        fontSize: 23,
      ),
    );
  }

  Container emptyContainer() {
    return Container(
      width: SizeConfig.scaleWidth(70),
      height: SizeConfig.scaleHeight(72),
      color: Colors.transparent,
    );
  }

  SizedBox marginHeight() {
    return SizedBox(height: SizeConfig.scaleHeight(24));
  }

  void fillPinCode() {
    if (pinCode.length == 1) {
      AuthController.to.addPin(num: pinCode.substring(0, 1), pin: 'pin1');
    } else if (pinCode.length == 2) {
      AuthController.to.addPin(num: pinCode.substring(1, 2), pin: 'pin2');
    } else if (pinCode.length == 3) {
      AuthController.to.addPin(num: pinCode.substring(2, 3), pin: 'pin3');
    } else if (pinCode.length == 4) {
      AuthController.to.addPin(num: pinCode.substring(3, 4), pin: 'pin4');
    }
  }

  void deletePinCode() {
    if (pinCode.length == 0) {
      AuthController.to.deletePin(num: '', pin: 'pin1');
    } else if (pinCode.length == 1) {
      AuthController.to.deletePin(num: '', pin: 'pin2');
    } else if (pinCode.length == 2) {
      AuthController.to.deletePin(num: '', pin: 'pin3');
    } else if (pinCode.length == 3) {
      AuthController.to.deletePin(num: '', pin: 'pin4');
    }
  }

  void goToLoginScreen() {
    Get.offAllNamed('/sign_in_screen');
  }

  void goToResetPassword() {
    Get.offAll(ResetPasswordScreen(code: widget.code!, mobile: widget.mobile!));
  }

  void accountActivationProcess() async {
    setState(() {
      isLoading = true;
    });
    Map<String, dynamic> dataVerification = Map<String, dynamic>();
    dataVerification['mobile'] = int.parse(widget.mobile!);
    dataVerification['code'] = int.parse(widget.code!);
    bool verificated =
        await AuthController().verificationAccount(context, dataVerification);
    if (verificated) {
      setState(() {
        isLoading = false;
      });
      goToLoginScreen();
    }
    setState(() {
      isLoading = false;
    });
  }

  void codeSendingProcess() {
    if (preformVerification()) {
      if (widget.title == 'signUp') {
        accountActivationProcess();
      } else {
        setState(() {
          isLoading = false;
        });
        if (pinCode == widget.code) goToResetPassword();
      }
    }
  }

}
