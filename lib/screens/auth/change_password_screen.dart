import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  bool isLoading = false;

  TextEditingController currentPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();

  @override
  void dispose() {
    currentPasswordController.dispose();
    confirmPasswordController.dispose();
    newPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      opacity: .2,
      color: AppColors.BLACK,
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
      ),
      child: Scaffold(
        backgroundColor: AppColors.BG_SCREENS_HOME,
        appBar: AppBar(
          title: MyText(
            title: AppLocalizations.of(context)!.changePassword,
            color: AppColors.TEXT_Bold,
            fontSize: 22,
            fontWeight: FontWeight.w700,
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          leading: Padding(
            padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
            child: MyContainerIconBack(
              onTap: () => Get.back(),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(30)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.currentPassword,
                  obscure: true,
                  controller: currentPasswordController,
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.newPassword,
                  controller: newPasswordController,
                  obscure: true,
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.confirmPassword,
                  controller: confirmPasswordController,
                  obscure: true,
                ),
                SizedBox(height: SizeConfig.scaleHeight(50)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.changePassword,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () {
                    preformChangePassword();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void preformChangePassword() {
    if (checkData()) {
      setState(() {
        isLoading = true;
      });
      changePassword();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool checkData() {
    if (currentPasswordController.text.isNotEmpty &&
        newPasswordController.text.isNotEmpty &&
        confirmPasswordController.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  void changePassword() async {
    bool changed = await AuthController().changePassword(context, {
      'current_password': currentPasswordController.text,
      'new_password': newPasswordController.text,
      'new_password_confirmation': confirmPasswordController.text,
    });
    if (changed) {
      setState(() {
        isLoading = false;
      });
      Get.back();
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }
}
