import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/api/controllers_api/city_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/model/city.dart';
import 'package:store_app/model/radio_model.dart';
import 'package:store_app/model/user.dart';
import 'package:store_app/screens/auth/verification_screen.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/icon_social_media.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> with MixinSittingsApp {
  City? citySelected;
  bool citySelected1 = true;

  late TextEditingController nameController;
  late TextEditingController mobileController;
  late TextEditingController passwordController;

  CityController controller = Get.put(CityController());
  AuthController controllerAuth = Get.put(AuthController());

  @override
  void initState() {
    nameController = TextEditingController();
    mobileController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    mobileController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREEN,
      body: Obx(() {
        return LoadingOverlay(
          isLoading: controllerAuth.isLoading.value,
          opacity: .2,
          color: AppColors.BLACK,
          progressIndicator: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.scaleWidth(18),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.scaleHeight(58)),
                  MyContainerIconBack(
                    onTap: () {
                      Get.back();
                    },
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(21)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                        title: AppLocalizations.of(context)!.signUp,
                        fontSize: 32,
                        color: AppColors.TEXT_Bold,
                        textAlign: TextAlign.start,
                        height: 1,
                      ),
                      MyTextButton(
                        title: AppLocalizations.of(context)!.signIn,
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        bgColor: AppColors.BG_TEXT_SIGN_UP,
                        colorText: AppColors.TEXT_SIGN_UP,
                        onPressed: () {
                          Get.back();
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(15)),
                  SizedBox(
                    width: double.infinity,
                    child: MyText(
                      title: AppLocalizations.of(context)!.pleaseSignUp,
                      fontSize: 18,
                      color: AppColors.FIRST_TEXT_LOGIN,
                      textAlign: TextAlign.start,
                      height: 1,
                    ),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(22)),
                  MyTextField(
                    keyboardType: TextInputType.text,
                    label: AppLocalizations.of(context)!.fillName,
                    obscure: false,
                    controller: nameController,
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(16)),
                  MyTextField(
                    keyboardType: TextInputType.number,
                    label: AppLocalizations.of(context)!.phoneNumber,
                    controller: mobileController,
                    obscure: false,
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(16)),
                  MyTextField(
                    keyboardType: TextInputType.text,
                    label: AppLocalizations.of(context)!.password,
                    controller: passwordController,
                    obscure: true,
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(16)),
                  SizedBox(
                    width: double.infinity,
                    child: MyText(
                      title: AppLocalizations.of(context)!.city,
                      color: AppColors.LABEL_HINT,
                      fontSize: 14,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(7)),
                  MyContainerShape(
                    height: SizeConfig.scaleHeight(60),
                    width: double.infinity,
                    paddingHorizontal: 14,
                    enableRadius: true,
                    borderRadius: 8,
                    enableShadow: false,
                    enableBorder: true,
                    colorBorder: AppColors.LABEL_HINT.withOpacity(.2),
                    paddingVertical: 0,
                    alignment: AlignmentDirectional.center,
                    bgContainer: AppColors.WHITE,
                    child: dropDownList(),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(16)),
                  Container(
                    height: SizeConfig.scaleHeight(45),
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        radioItem(RadioModel(
                            AppLocalizations.of(context)!.male,
                            controllerAuth.radioItem1.value)),
                        SizedBox(width: SizeConfig.scaleWidth(16)),
                        radioItem(RadioModel(
                            AppLocalizations.of(context)!.female,
                            controllerAuth.radioItem2.value)),
                      ],
                    ),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(30)),
                  MyTextButton(
                    title: AppLocalizations.of(context)!.getStarted,
                    bgColor: AppColors.BG_BUTTON,
                    colorText: AppColors.WHITE,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    enabledSize: true,
                    height: 55,
                    onPressed: () {
                      preformSignUp();
                    },
                  ),
                  // SizedBox(height: SizeConfig.scaleHeight(28)),
                  // Row(
                  //   children: [
                  //     Expanded(child: Divider()),
                  //     MyText(
                  //       title: '  Login with social   ',
                  //       fontSize: 16,
                  //       color: AppColors.FIRST_TEXT_LOGIN,
                  //     ),
                  //     Expanded(child: Divider()),
                  //   ],
                  // ),
                  // SizedBox(height: SizeConfig.scaleHeight(35)),
                  // Row(
                  //   children: [
                  //     IconSocialMedia(
                  //       bgColor: AppColors.BG_ICON_FACEBOOK,
                  //       icon: 'images/facebook.png',
                  //       onTap: () {
                  //         // loginFacebook();
                  //       },
                  //     ),
                  //     SizedBox(width: SizeConfig.scaleWidth(25)),
                  //     IconSocialMedia(
                  //       icon: 'images/google.png',
                  //       bgColor: AppColors.BG_ICON_GOOGLE,
                  //       onTap: () {
                  //         loginGoogle();
                  //       },
                  //     ),
                  //   ],
                  // ),
                  SizedBox(height: SizeConfig.scaleHeight(50))
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  radioItem(RadioModel _item) {
    return GestureDetector(
      onTap: () {
        getColorRadioItem(_item.title);
        if (_item.title == AppLocalizations.of(context)!.male) {
          controllerAuth.changeGender('M');
        } else {
          controllerAuth.changeGender('F');
        }
      },
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              margin: EdgeInsetsDirectional.only(end: 10.0),
              child: MyText(
                title: _item.title,
                color: AppColors.TEXT_Bold,
                fontWeight: FontWeight.w500,
              ),
            ),
            Container(
              height: SizeConfig.scaleHeight(25),
              width: SizeConfig.scaleWidth(25),
              child: Center(
                child: Visibility(
                  visible: _item.selected,
                  child: Icon(
                    Icons.check,
                    color: AppColors.WHITE,
                    size: 18,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color:
                _item.selected ? AppColors.BG_BUTTON : Colors.transparent,
                border: Border.all(
                  width: 1.0,
                  color: _item.selected ? AppColors.BG_BUTTON : Colors.grey,
                ),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(
                    6.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getColorRadioItem(String title) {
    if (title == AppLocalizations.of(context)!.male) {
      controllerAuth.changeRadioButton(true, false);
    } else {
      controllerAuth.changeRadioButton(false, true);
    }
  }

  Widget dropDownList() {
    return Obx(
          () {
        if (controller.cities.isNotEmpty) {
          if (citySelected1) {
            citySelected = controller.cities.first;
            citySelected1 = false;
          }
          return DropdownButtonFormField<City>(
            isDense: true,
            value: citySelected,
            decoration: InputDecoration(
              border: InputBorder.none,
            ),
            onChanged: (City? city) {
              citySelected = city;
            },
            items: controller.cities.map((City value) {
              return DropdownMenuItem<City>(
                value: value,
                child: MyText(
                    title: SharedPrefController().getLangCode == 'en' ? value
                        .nameEn : value.nameAr, fontSize: 16),
              );
            }).toList(),
          );
        } else {
          return Align(
            alignment: AlignmentDirectional.centerEnd,
            child: SizedBox(
              height: SizeConfig.scaleHeight(14),
              width: SizeConfig.scaleWidth(14),
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
              ),
            ),
          );
        }
      },
    );
  }

  User get user {
    return User(
      id: 0,
      cityId: citySelected!.id,
      gender: controllerAuth.gender.value,
      mobile: mobileController.text,
      name: nameController.text,
      password: passwordController.text,
      storeApiKey: 'ea051906-3f05-4851-a8d6-d97b6b98c5bf',
    );
  }

  void preformSignUp() {
    bool check = checkData();
    if (check) {
      numberMobile();
      signUp();
    } else
      Helpers.showSnackBar(context,
          message: AppLocalizations.of(context)!.checkFields,
          error: true);
  }

  bool checkData() {
    if (citySelected != null &&
        controllerAuth.gender.value != '' &&
        mobileController.text != '' &&
        nameController.text != '' &&
        passwordController.text != '') {
      return true;
    }
    return false;
  }

  void signUp() async {
    controllerAuth.changeIsLoading(true);
    int code = await AuthController().registerAccount(context, user);
    print(code);
    if (code > 0) {
      Get.to(
        VerificationScreen(
          title: 'signUp',
          code: code.toString(),
          mobile: mobileController.text,
        ),
      );
    }
    controllerAuth.changeIsLoading(false);
  }

  numberMobile() {
    if (mobileController.text.length == 10) {
      mobileController.text = mobileController.text.substring(1, 10);
    }
  }
}
