import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/icon_social_media.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  late TextEditingController mobileController;
  late TextEditingController passwordController;

  bool isLoading = false;

  @override
  void initState() {
    mobileController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    mobileController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREEN,
      body: LoadingOverlay(
        isLoading: isLoading,
        opacity: .2,
        color: AppColors.BLACK,
        progressIndicator: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(18),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(111)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MyText(
                      title: AppLocalizations.of(context)!.signIn,
                      fontSize: 32,
                      color: AppColors.TEXT_Bold,
                      textAlign: TextAlign.start,
                      height: 1,
                    ),
                    MyTextButton(
                      title: AppLocalizations.of(context)!.signUp,
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      bgColor: AppColors.BG_TEXT_SIGN_UP,
                      colorText: AppColors.TEXT_SIGN_UP,
                      onPressed: () {
                        Get.toNamed('/sign_up_screen');
                      },
                    ),
                  ],
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                SizedBox(
                  width: double.infinity,
                  child: MyText(
                    title: AppLocalizations.of(context)!.firstLogin,
                    fontSize: 18,
                    color: AppColors.FIRST_TEXT_LOGIN,
                    textAlign: TextAlign.start,
                    height: 1,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(22)),
                MyTextField(
                  keyboardType: TextInputType.number,
                  label: AppLocalizations.of(context)!.phoneNumber,
                  controller: mobileController,
                  obscure: false,
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.password,
                  controller: passwordController,
                  obscure: true,
                ),
                SizedBox(height: SizeConfig.scaleHeight(24)),
                SizedBox(
                  width: double.infinity,
                  child: GestureDetector(
                    onTap: () {
                      Get.toNamed('/forgot_password_screen');
                    },
                    child: MyText(
                      title: AppLocalizations.of(context)!.forgotPassword1,
                      fontSize: 16,
                      color: AppColors.TEXT_FORGET_PASSWORD,
                      textAlign: TextAlign.end,
                      fontWeight: FontWeight.w500,
                      height: 1,
                    ),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(25)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.getStarted,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () async{

                    preformSignIn();
                  },
                ),
                // SizedBox(height: SizeConfig.scaleHeight(28)),
                // Row(
                //   children: [
                //     Expanded(child: Divider()),
                //     MyText(
                //       title: AppLocalizations.of(context)!.'  Login with social   ',
                //       fontSize: 16,
                //       color: AppColors.FIRST_TEXT_LOGIN,
                //     ),
                //     Expanded(child: Divider()),
                //   ],
                // ),
                // SizedBox(height: SizeConfig.scaleHeight(35)),
                // Row(
                //   children: [
                //     IconSocialMedia(
                //       bgColor: AppColors.BG_ICON_FACEBOOK,
                //       icon: 'images/facebook.png',
                //       onTap: () {
                //         loginFacebook();
                //       },
                //     ),
                //     SizedBox(width: SizeConfig.scaleWidth(25)),
                //     IconSocialMedia(
                //       icon: 'images/google.png',
                //       bgColor: AppColors.BG_ICON_GOOGLE,
                //       onTap: () {
                //         loginGoogle();
                //       },
                //     ),
                //   ],
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void preformSignIn() {
    setState(() {
      isLoading = true;
    });
    if (checkData()) {
      numberMobile();
      signIn();
    } else {
      setState(() {
        isLoading = false;
      });
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool checkData() {
    if (mobileController.text.isNotEmpty && passwordController.text.isNotEmpty)
      return true;
    return false;
  }

  void signIn() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    Map<String, dynamic> mapData = Map<String, dynamic>();
    mapData['mobile'] = mobileController.text;
    mapData['password'] = passwordController.text;
    mapData['fcm_token'] = await messaging.getToken();
    bool check = await AuthController().signInAccount(context, mapData);
    setState(() {
      isLoading = false;
    });
    if (check) {
      Get.offAllNamed('/main_bottom_navigation_screen');
    }
  }

  numberMobile() {
    if (mobileController.text.length == 10) {
      mobileController.text = mobileController.text.substring(1, 10);
    }
  }
}
