import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/screens/auth/verification_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  late TextEditingController mobileController = TextEditingController();

  bool isLoading = false;

  @override
  void dispose() {
    mobileController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      opacity: .2,
      color: AppColors.BLACK,
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
      ),
      child: Scaffold(
        backgroundColor: AppColors.BG_SCREEN,
        body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(18),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(58)),
                MyContainerIconBack(
                  onTap: () {
                    Get.back();
                  },
                ),
                SizedBox(height: SizeConfig.scaleHeight(21)),
                SizedBox(
                  width: double.infinity,
                  child: MyText(
                    title: AppLocalizations.of(context)!.forgotPassword,
                    fontSize: 32,
                    color: AppColors.TEXT_Bold,
                    textAlign: TextAlign.start,
                    height: 1,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                SizedBox(
                  width: double.infinity,
                  child: MyText(
                    title: AppLocalizations.of(context)!.enterPhoneNum,
                    fontSize: 18,
                    color: AppColors.FIRST_TEXT_LOGIN,
                    textAlign: TextAlign.start,
                    height: 1,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(40)),
                MyTextField(
                  keyboardType: TextInputType.number,
                  label: AppLocalizations.of(context)!.enterPhoneNum1,
                  obscure: false,
                  controller: mobileController,
                ),
                SizedBox(height: SizeConfig.scaleHeight(50)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.sendCode,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () {
                    preformSubmitSendVerification();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void preformSubmitSendVerification() {
    if (checkData()) {
      setState(() {
        isLoading = true;
      });
      numberMobile();
      sendCodeForget();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool checkData() {
    if (mobileController.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  numberMobile() {
    if (mobileController.text.length == 10) {
      mobileController.text = mobileController.text.substring(1, 10);
    }
  }

  void sendCodeForget() async {
    int sendCode = await AuthController()
        .forgetPassword(context, int.parse(mobileController.text));
    if (sendCode > 0) {
      setState(() {
        isLoading = false;
      });
      Get.to(VerificationScreen(
        title: AppLocalizations.of(context)!.resetPassword,
        mobile: mobileController.text,
        code: sendCode.toString(),
      ));
    }else{
      setState(() {
        isLoading = false;
      });
    }
  }
}
