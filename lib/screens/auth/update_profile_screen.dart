import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/api/controllers_api/city_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/city.dart';
import 'package:store_app/model/radio_model.dart';
import 'package:store_app/screens/other_controller/controller_username.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_icon_back.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UpdateProfileScreen extends StatefulWidget {
  @override
  _UpdateProfileScreenState createState() => _UpdateProfileScreenState();
}

class _UpdateProfileScreenState extends State<UpdateProfileScreen> {
  bool isLoading = false;
  City? citySelected;
  bool citySelected1 = true;

  bool male = false;
  bool female = false;

  CityController controllerCity = Get.put(CityController());

  TextEditingController nameController = TextEditingController();

  String gender = '';

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    nameController.text = SharedPrefController().getNameUser;
    if (SharedPrefController().getGenderUser == 'M') {
      male = true;
      female = false;
      gender = 'M';
    } else {
      female = true;
      male = false;
      gender = 'F';
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      opacity: .2,
      color: AppColors.BLACK,
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
      ),
      child: Scaffold(
        backgroundColor: AppColors.BG_SCREENS_HOME,
        appBar: AppBar(
          title: MyText(
            title: AppLocalizations.of(context)!.updateProfile,
            color: AppColors.TEXT_Bold,
            fontSize: 22,
            fontWeight: FontWeight.w700,
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          leading: Padding(
            padding: EdgeInsetsDirectional.only(start: SizeConfig.scaleWidth(20)),
            child: MyContainerIconBack(
              onTap: () => Get.back(),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(30)),
                MyContainerShape(
                  width: 90,
                  height: SizeConfig.scaleHeight(90),
                  enableBorder: false,
                  enableShadow: false,
                  enableRadius: true,
                  alignment: AlignmentDirectional.center,
                  paddingHorizontal: 0,
                  paddingVertical: 0,
                  borderRadius: 12,
                  bgContainer: AppColors.WHITE,
                  child: Icon(
                    Icons.person,
                    size: SizeConfig.scaleWidth(55),
                    color: AppColors.FIRST_TEXT_LOGIN,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(20)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.newName,
                  obscure: false,
                  controller: nameController,
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyContainerShape(
                  height: SizeConfig.scaleHeight(60),
                  width: double.infinity,
                  paddingHorizontal: 14,
                  enableRadius: true,
                  borderRadius: 4,
                  enableShadow: false,
                  enableBorder: true,
                  colorBorder: AppColors.LABEL_HINT.withOpacity(.2),
                  paddingVertical: 0,
                  alignment: AlignmentDirectional.center,
                  bgContainer: AppColors.WHITE,
                  child: dropDownList(),
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                Container(
                  height: SizeConfig.scaleHeight(45),
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      radioItem(
                          RadioModel(AppLocalizations.of(context)!.male, male)),
                      SizedBox(width: SizeConfig.scaleWidth(16)),
                      radioItem(RadioModel(
                          AppLocalizations.of(context)!.female, female)),
                    ],
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.updateProfile,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () {
                    preformChangePassword();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  radioItem(RadioModel _item) {
    return GestureDetector(
      onTap: () {
        getColorRadioItem(_item.title);
        if (_item.title == AppLocalizations.of(context)!.male) {
          gender = 'M';
          male = true;
          female = false;
        } else {
          gender = 'F';
          male = false;
          female = true;
        }
        setState(() {});
      },
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              margin: EdgeInsetsDirectional.only(end: 10.0),
              child: MyText(
                title: _item.title,
                color: AppColors.TEXT_Bold,
                fontWeight: FontWeight.w500,
              ),
            ),
            Container(
              height: SizeConfig.scaleHeight(25),
              width: SizeConfig.scaleWidth(25),
              child: Center(
                child: Visibility(
                  visible: _item.selected,
                  child: Icon(
                    Icons.check,
                    color: AppColors.WHITE,
                    size: 18,
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color:
                    _item.selected ? AppColors.BG_BUTTON : Colors.transparent,
                border: Border.all(
                  width: 1.0,
                  color: _item.selected ? AppColors.BG_BUTTON : Colors.grey,
                ),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(
                    6.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getColorRadioItem(String title) {
    if (title == AppLocalizations.of(context)!.male) {
      gender = 'M';
    } else {
      gender = 'F';
    }
  }

  Widget dropDownList() {
    return Obx(
      () {
        if (controllerCity.cities.isNotEmpty) {
          int cityId = SharedPrefController().getCityIdUser;
          if (citySelected1) {
            for (int i = 0; i < controllerCity.cities.length; i++) {
              if (cityId == controllerCity.cities[i].id) {
                citySelected = controllerCity.cities[i];
              }
            }
            citySelected1 = false;
          }
          return DropdownButtonFormField<City>(
            isDense: true,
            value: citySelected,
            decoration: InputDecoration(
              border: InputBorder.none,
            ),
            onChanged: (City? city) {
              citySelected = city;
            },
            items: controllerCity.cities.map((City value) {
              return DropdownMenuItem<City>(
                value: value,
                child: MyText(
                    title: SharedPrefController().getLangCode == 'en'
                        ? value.nameEn
                        : value.nameAr,
                    fontSize: 16),
              );
            }).toList(),
          );
        } else {
          return Align(
            alignment: AlignmentDirectional.centerEnd,
            child: SizedBox(
              height: SizeConfig.scaleHeight(14),
              width: SizeConfig.scaleWidth(14),
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
              ),
            ),
          );
        }
      },
    );
  }

  void preformChangePassword() {
    if (checkData()) {
      setState(() {
        isLoading = true;
      });
      updateProfile();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
    print(checkData());
  }

  bool checkData() {
    if (nameController.text.isNotEmpty &&
        citySelected != null &&
        gender.isNotEmpty) {
      return true;
    }
    return false;
  }

  void updateProfile() async {
    bool updated = await AuthController().updateProfile(context, {
      'name': nameController.text,
      'gender': gender,
      'city_id': citySelected!.id
    });
    if (updated) {
      UsernameController.to.changeUserName(nameController.text);
      await SharedPrefController().setAddressId(citySelected!.id);
      await SharedPrefController().setUserName(nameController.text);
      await SharedPrefController().setAddressName(
          addressEn: citySelected!.nameEn, addressAr: citySelected!.nameAr);
      setState(() {
        isLoading = false;
      });
      Get.back();
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }
}
