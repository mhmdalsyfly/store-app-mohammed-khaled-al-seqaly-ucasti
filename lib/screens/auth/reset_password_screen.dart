import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:store_app/api/controllers_api/auth_controller.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_text.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:store_app/widgets/my_text_field.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ResetPasswordScreen extends StatefulWidget {
  String code;
  String mobile;

  ResetPasswordScreen({required this.code, required this.mobile});

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  bool isLoading = false;

  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  @override
  void dispose() {
    newPasswordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      opacity: .2,
      color: AppColors.BLACK,
      progressIndicator: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
      ),
      child: Scaffold(
        backgroundColor: AppColors.BG_SCREEN,
        body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(18),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.scaleHeight(111)),
                SizedBox(
                  width: double.infinity,
                  child: MyText(
                    title: AppLocalizations.of(context)!.resetPassword,
                    fontSize: 32,
                    color: AppColors.TEXT_Bold,
                    textAlign: TextAlign.start,
                    height: 1,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                SizedBox(
                  width: double.infinity,
                  child: MyText(
                    title: AppLocalizations.of(context)!.pleaseSignUp,
                    fontSize: 18,
                    color: AppColors.FIRST_TEXT_LOGIN,
                    textAlign: TextAlign.start,
                    height: 1,
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(40)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.newPassword,
                  obscure: true,
                  controller: newPasswordController,
                ),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyTextField(
                  keyboardType: TextInputType.text,
                  label: AppLocalizations.of(context)!.confirmPassword,
                  controller: confirmPasswordController,
                  obscure: true,
                ),
                SizedBox(height: SizeConfig.scaleHeight(50)),
                MyTextButton(
                  title: AppLocalizations.of(context)!.updatePassword,
                  bgColor: AppColors.BG_BUTTON,
                  colorText: AppColors.WHITE,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  enabledSize: true,
                  height: 55,
                  onPressed: () {
                    preformUpdatePassword();
                    // TODO:: Navigate to signIn.
                    // Get.offNamed('/sign_in_screen');
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void preformUpdatePassword() {
    if (checkData()) {
      if (newPasswordController.text == confirmPasswordController.text) {
        setState(() {
          isLoading = true;
        });
        updatePassword();
      } else {
        Helpers.showSnackBar(
          context,
          message: AppLocalizations.of(context)!.passwordNotMatch,
          error: true,
        );
      }
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.checkFields,
        error: true,
      );
    }
  }

  bool checkData() {
    if (newPasswordController.text.isNotEmpty &&
        confirmPasswordController.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  void updatePassword() async {
    bool reset = await AuthController().resetPassword(context, {
      'mobile': widget.mobile,
      'code': widget.code,
      'password': newPasswordController.text,
      'password_confirmation': confirmPasswordController.text,
    });
    if (reset) {
      setState(() {
        isLoading = false;
      });
      Get.offAllNamed('/sign_in_screen');
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }
}
