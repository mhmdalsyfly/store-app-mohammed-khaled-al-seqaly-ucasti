import 'package:shared_preferences/shared_preferences.dart';
import 'package:store_app/model/user.dart';
import 'package:store_app/storage/pref/pref_keys.dart';

class SharedPrefController {
  static final SharedPrefController _instance =
      SharedPrefController._internal();

  late SharedPreferences _preferences;

  SharedPrefController._internal();

  factory SharedPrefController() => _instance;

  Future<SharedPreferences> initSharedPref() async =>
      _preferences = await SharedPreferences.getInstance();

  SharedPreferences get prefManager => _preferences;

  Future<bool> setUser(User user) async {
    await _preferences.setInt(PrefKeys.ID, user.id);
    await _preferences.setInt(PrefKeys.CITY_ID, user.cityId);
    await _preferences.setString(PrefKeys.NAME, user.name);
    await _preferences.setString(PrefKeys.MOBILE, user.mobile);
    await _preferences.setString(PrefKeys.STORE_API_KEY, user.storeApiKey);
    return await _preferences.setString(PrefKeys.GENDER, user.gender);
  }

  Future<bool> setAddressId(int id) async {
    return await _preferences.setInt(PrefKeys.CITY_ID, id);
  }

  Future<bool> setUserName(String name) async {
    return await _preferences.setString(PrefKeys.NAME, name);
  }

  Future<bool> setAddressName(
      {required String addressEn, required String addressAr}) async {
    await _preferences.setString(PrefKeys.CITY_EN, addressEn);
    return await _preferences.setString(PrefKeys.CITY_AR, addressAr);
  }

  int get getCityIdUser => _preferences.getInt(PrefKeys.CITY_ID) ?? -1;

  int get getId => _preferences.getInt(PrefKeys.ID) ?? -1;

  String get getNameUser => _preferences.getString(PrefKeys.NAME) ?? '';

  String get getMobileUser => _preferences.getString(PrefKeys.MOBILE) ?? '';

  String get getStoreApiKey =>
      _preferences.getString(PrefKeys.STORE_API_KEY) ?? '';

  String get getLangCode =>
      _preferences.getString(PrefKeys.LANG_CODE_KEY) ?? 'en';

  String get getGenderUser => _preferences.getString(PrefKeys.GENDER) ?? '';

  Future<bool> setCityArUser(String cityAr) async {
    return await _preferences.setString(PrefKeys.CITY_AR, cityAr);
  }

  Future<bool> setCityEnUser(String cityEn) async {
    return await _preferences.setString(PrefKeys.CITY_EN, cityEn);
  }

  Future<bool> setToken(String token) async {
    return await _preferences.setString(PrefKeys.TOKEN, token);
  }

  Future<bool> setTokenType(String tokenType) async {
    return await _preferences.setString(PrefKeys.TOKEN_TYPE, tokenType);
  }

  Future<bool> setLangCode(String value) async =>
      await _preferences.setString(PrefKeys.LANG_CODE_KEY, value);

  String get getCityArUser => _preferences.getString(PrefKeys.CITY_AR) ?? '';

  String get getCityEnUser => _preferences.getString(PrefKeys.CITY_EN) ?? '';

  String get getToken => _preferences.getString(PrefKeys.TOKEN) ?? '';

  String get getTokenType => _preferences.getString(PrefKeys.TOKEN_TYPE) ?? '';

  Future<void> clearDataFromSharedPre() async {
    SharedPrefController().setUser(User(
      id: -1,
      cityId: -1,
      gender: '',
      mobile: '',
      name: '',
      password: '',
      storeApiKey: '',
    ));
    SharedPrefController().setAddressName(addressEn: '', addressAr: '');
    SharedPrefController().setToken('');
    SharedPrefController().setTokenType('');
    SharedPrefController().setAddressId(-1);
    SharedPrefController().setUserName('');
  }
}
