class PrefKeys {
  static const String LANG_CODE_KEY = 'langeCode';

  static const String NAME = 'name';
  static const String MOBILE = 'mobile';
  static const String GENDER = 'gender';
  static const String STORE_API_KEY = 'storeApiKey';
  static const String CITY_ID = 'city_id';
  static const String ID = 'id';

  static const String CITY_AR = 'cityAr';
  static const String CITY_EN = 'cityEn';
  static const String TOKEN_TYPE = 'tokenType';
  static const String TOKEN = 'token';
}
