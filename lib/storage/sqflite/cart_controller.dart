import 'package:sqflite/sqflite.dart';
import 'package:store_app/model/cart.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';

import 'db_operations.dart';
import 'db_provider.dart';

class CartController extends DBOperations<Cart> {
  Database _database;

  CartController() : _database = DBProvider().database;

  @override
  Future<int> create(Cart cart) async {
    int row = await _database.insert(DBProvider.TABLE_CART, cart.toJson());
    return row;
  }

  @override
  Future<bool> delete(int id) async {
    return await _database.delete(
          DBProvider.TABLE_CART,
          where: 'idCart = ? and idUser = ?',
          whereArgs: [id, SharedPrefController().getId],
        ) >
        0;
  }

  @override
  Future<bool> update(Cart cart) async {
    int row = await _database.update(DBProvider.TABLE_CART, cart.toJson(),
        where: 'idCart = ? and idUser = ?',
        whereArgs: [cart.idCart, SharedPrefController().getId]);
    return row != 0;
  }

  @override
  Future<List<Cart>> read() async {
    List<Map<String, Object?>> listMap = await _database.query(
        DBProvider.TABLE_CART,
        where: 'idUser = ?',
        whereArgs: [SharedPrefController().getId]);
    return listMap.map((rowUserMap) => Cart.fromJson(rowUserMap)).toList();
  }

  @override
  Future<bool> deleteAll() async {
    return await _database.delete(DBProvider.TABLE_CART,
            where: 'idUser=?', whereArgs: [SharedPrefController().getId]) >
        0;
  }

// Future<bool> clearDataUser() async {
//   return await _database.delete(DBProvider.TABLE_CART,
//       where: 'user_id=?',
//       whereArgs: [SharedPrefController().getUser().id]) >
//       0;
// }

// @override
// Future<List<Category>> readCategoryIncome() async {
//   List<Map<String, Object?>> listMap = await _database.rawQuery(
//       'SELECT categories.id, categories.user_id, categories.name,categories.expense, count(actions.id) AS count_action '
//           'FROM categories '
//           'LEFT JOIN actions '
//           'ON categories.id = actions.category_id '
//           'WHERE categories.expense = 0 and categories.user_id = ${SharedPrefController().getUser().id} '
//           'GROUP BY categories.id');
//   return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
// }
//
// @override
// Future<List<Category>> readCategoryExpenses() async {
//   List<Map<String, Object?>> listMap = await _database.rawQuery(
//       'SELECT categories.id, categories.user_id, categories.name,categories.expense, count(actions.id) AS count_action '
//           'FROM categories '
//           'LEFT JOIN actions '
//           'ON categories.id = actions.category_id '
//           'WHERE categories.expense = 1 and categories.user_id = ${SharedPrefController().getUser().id} '
//           'GROUP BY categories.id');
//   return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
// }

// @override
// Future<List<Category>> readCategoryExpenses() async {
//   List<Map<String, Object?>> listMap = await _database
//       .query(DBConst.TABLE_CATRGORIES, where: 'expense = ?', whereArgs: [1]);
//   return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
// }
//
// @override
// Future<List<Category>> readCategoryIncome() async {
//   List<Map<String, Object?>> listMap = await _database
//       .query(DBConst.TABLE_CATRGORIES, where: 'expense = ?', whereArgs: [0]);
//   return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
// }
}
