import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/cart.dart';
import 'package:store_app/model/get_order.dart';
import 'package:store_app/model/order.dart';
import 'package:store_app/model/send_cart.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/storage/sqflite/cart_controller.dart';

import '../sittings_api.dart';

class CartControllerGetx extends GetxController {
  RxList<Cart> listProducts = <Cart>[].obs;
  RxList<SendCart> listCart = <SendCart>[].obs;
  BuildContext context;
  RxBool isLoading = false.obs;

  static CartControllerGetx get to => Get.find();
  late CartController cartController = CartController();

  CartControllerGetx({required this.context});

  @override
  void onInit() {
    read();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    listProducts.clear();
    listCart.clear();
    super.onClose();
  }

  Future<int> create(Cart cart) async {
    int value = await cartController.create(cart);
    print(cart.idUser);
    if (value > 0) {
      cart.idCart = value;
      addProduct(cart);
    }
    return value;
  }

  Future<void> read() async {
    isLoading.value = true;
    listProducts.value = await cartController.read();
    for (int i = 0; i < listProducts.length; i++) {
      listCart.add(
        SendCart(
          productId: listProducts[i].idProduct,
          quantity: listProducts[i].requiredQuantity,
        ),
      );
    }
    isLoading.value = false;
    print(isLoading.value);
  }

  addQuantity(int index) async {
    listProducts[index].requiredQuantity++;
    listCart[index].quantity++;
    listProducts.refresh();
    listCart.refresh();

    await cartController.update(listProducts[index]);
  }

  decreaseQuantity(int index) async {
    listProducts[index].requiredQuantity--;
    listCart[index].quantity--;
    listProducts.refresh();
    listCart.refresh();
    await cartController.update(listProducts[index]);
  }

  addProduct(Cart product) {
    listProducts.add(product);
    listCart.add(
      SendCart(
        productId: product.idProduct,
        quantity: product.requiredQuantity,
      ),
    );
  }

  deleteProduct(Cart cart, int index) async {
    bool deleted = await cartController.delete(cart.idCart!);
    if (deleted) {
      listProducts.removeAt(index);
    }
  }

  clearData() async {
    bool deleted = await cartController.deleteAll();
    if (deleted) {
      listProducts.clear();
      listCart.clear();
    }
    Helpers.showSnackBar(
      context,
      message: deleted
          ? AppLocalizations.of(context)!.basketDeleted
          : AppLocalizations.of(context)!.problemDeletion,
      error: !deleted,
    );
  }

  Future<bool> sendOrder(BuildContext context, Order order) async {
    final response = await http.post(
      Uri.parse(SittingsApi.ORDER),
      headers: <String, String>{
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
      body: order.toJson(),
    );

    print(order.toJson());

    if (response.statusCode == 200) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<List<GetOrder>> getAllOrders() async {
    List<GetOrder> listOrder = [];
    isLoading.value = true;
    var response = await http.get(
      Uri.parse(SittingsApi.ORDER),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
    );

    print(response.statusCode);

    if (response.statusCode == 200) {
      var ordersJson = jsonDecode(response.body)['list'] as List;
      listOrder = ordersJson
          .map((orderObjectJson) => GetOrder.fromJson(orderObjectJson))
          .toList();
      isLoading.value = false;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
      isLoading.value = false;
    }

    return listOrder;
  }

  Future<Map<String, dynamic>> getDetailsOrder(int idOrder) async {
    Map<String, dynamic> orderDetails = Map<String, dynamic>();

    var response = await http.get(
      Uri.parse(SittingsApi.ORDER + '/' + idOrder.toString()),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
    );

    if (response.statusCode == 200) {
      var dataJson = jsonDecode(response.body)['data'];
      var dataAddressJson = dataJson['address'];
      var dataAddressCityJson = dataAddressJson['city'];

      orderDetails['id'] = dataJson['id'];
      orderDetails['total'] = dataJson['total'];
      orderDetails['date'] = dataJson['date'];
      orderDetails['payment_type'] = dataJson['payment_type'];
      orderDetails['status'] = dataJson['status'];
      orderDetails['products_count'] = dataJson['products_count'];

      orderDetails['name_address'] = dataAddressJson['name'];
      orderDetails['info_address'] = dataAddressJson['info'];
      orderDetails['contact_number'] = dataAddressJson['contact_number'];

      orderDetails['name_en_city'] = dataAddressCityJson['name_en'];
      orderDetails['name_ar_city'] = dataAddressCityJson['name_ar'];

      orderDetails['products'] = dataJson['products'] as List;

      return orderDetails;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return orderDetails;
  }
}
