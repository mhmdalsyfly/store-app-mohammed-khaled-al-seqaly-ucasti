import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/address.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';

import '../sittings_api.dart';

class AddressController extends GetxController {
  RxList<Address> listAddress = <Address>[].obs;
  RxBool isLoading = false.obs;
  BuildContext context;

  static AddressController get to => Get.find();

  AddressController({required this.context});

  @override
  void onInit() {
    getAddress();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    listAddress.clear();
    super.onClose();
  }

  Future<List<Address>> getAddress() async {
    isLoading.value = true;
    var response = await http.get(
      Uri.parse(SittingsApi.ADDRESS),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
    );

    print(response.statusCode);

    if (response.statusCode == 200) {
      var addressJson = jsonDecode(response.body)['list'] as List;
      listAddress.value = addressJson
          .map((addressObjectJson) => Address.fromJson(addressObjectJson))
          .toList();
      isLoading.value = false;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
      isLoading.value = false;
    }

    return listAddress;
  }

  Future<bool> addAddress(BuildContext context, Address address) async {
    final response = await http.post(
      Uri.parse(SittingsApi.ADDRESS),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
      body: jsonEncode(dataAddressJson(address)),
    );

    if (response.statusCode == 201) {
      var allDataObject = jsonDecode(response.body)['object'];
      int id = allDataObject['id'];
      address.id = id;
      listAddress.add(address);

      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> editAddress(
      BuildContext context, Address address, int index) async {
    final response = await http.put(
      Uri.parse(SittingsApi.ADDRESS + '/' + address.id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
      body: jsonEncode(dataAddressJson(address)),
    );

    if (response.statusCode == 200) {
      listAddress.removeAt(index);
      listAddress.insert(index, address);
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    print(address.id.toString());
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> deleteAddress(BuildContext context, int idAddress) async {
    final response = await http.delete(
      Uri.parse(SittingsApi.ADDRESS + '/' + idAddress.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
    );

    if (response.statusCode == 200) {
      listAddress.removeWhere((item) => item.id == idAddress);

      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Map<String, dynamic> dataAddressJson(Address address) {
    return {
      'name': address.name,
      'info': address.info,
      'contact_number': address.contactNumber,
      'city_id': address.cityId,
    };
  }
}
