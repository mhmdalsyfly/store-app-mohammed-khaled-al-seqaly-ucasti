import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:store_app/api/sittings_api.dart';
import 'package:store_app/model/city.dart';

class CityController extends GetxController {
  RxList<City> cities = <City>[].obs;

  static CityController get to => Get.find();

  @override
  void onInit() {
    getCities();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }


  @override
  void onClose() {
    cities.clear();
    super.onClose();
  }

  Future<List<City>> getCities() async {
    var response = await http.get(
      Uri.parse(SittingsApi.GET_CITIES),
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
      },
    );

    print(response.statusCode);

    if (response.statusCode == 200) {
      var citiesJsonList = jsonDecode(response.body)['list'] as List;
      cities.value = citiesJsonList
          .map((taskObjectJson) => City.fromJson(taskObjectJson))
          .toList();
    }
    return cities;
  }

}
