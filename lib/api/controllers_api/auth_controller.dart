import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/user.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';

import '../sittings_api.dart';

class AuthController extends GetxController {
  RxString gender = ''.obs;

  RxBool isLoading = false.obs;
  RxBool radioItem1 = false.obs;
  RxBool radioItem2 = false.obs;

  RxString pin1 = ''.obs;
  RxString pin2 = ''.obs;
  RxString pin3 = ''.obs;
  RxString pin4 = ''.obs;

  addPin({required String num, required String pin}) {
    switch (pin) {
      case 'pin1':
        pin1.value = num;
        break;
      case 'pin2':
        pin2.value = num;
        break;
      case 'pin3':
        pin3.value = num;
        break;
      case 'pin4':
        pin4.value = num;
        break;
    }
  }

  deletePin({required String num, required String pin}) {
    switch (pin) {
      case 'pin1':
        pin1.value = '';
        break;
      case 'pin2':
        pin2.value = '';
        break;
      case 'pin3':
        pin3.value = '';
        break;
      case 'pin4':
        pin4.value = '';
        break;
    }
  }

  changeRadioButton(bool radioMale, bool radioFemale) {
    radioItem1.value = radioMale;
    radioItem2.value = radioFemale;
  }

  changeGender(String gen) {
    gender.value = gen;
  }

  changeIsLoading(bool status) {
    isLoading.value = status;
  }

  static AuthController get to => Get.find();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    isLoading.value = false;
    radioItem1.value = false;
    radioItem2.value = false;
    gender.value = '';
    super.onClose();
  }

  Future<int> registerAccount(BuildContext context, User user) async {
    final response = await http.post(
      Uri.parse(SittingsApi.REGISTER),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode(user.toJson()),
    );

    print(user.cityId);
    print(response.statusCode);

    if (response.statusCode == 201) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return jsonDecode(response.body)['code'];
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return 0;
  }

  Future<bool> signInAccount(
      BuildContext context, Map<String, dynamic> user) async {
    final response = await http.post(
      Uri.parse(SittingsApi.LOGIN),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode(user),
    );
    print(user['password']);

    if (response.statusCode == 200) {
      var allDataUser = jsonDecode(response.body)['data'];
      var dataCity = allDataUser['city'];

      await SharedPrefController().setUser(User(
        id: allDataUser['id'],
        storeApiKey: 'ea051906-3f05-4851-a8d6-d97b6b98c5bf',
        password: '',
        name: allDataUser['name'],
        mobile: allDataUser['mobile'],
        gender: allDataUser['gender'],
        cityId: allDataUser['city_id'],
      ));

      await SharedPrefController().setCityArUser(dataCity['name_ar']);
      await SharedPrefController().setCityEnUser(dataCity['name_en']);
      await SharedPrefController().setToken(allDataUser['token']);
      await SharedPrefController().setTokenType(allDataUser['token_type']);

      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> verificationAccount(
      BuildContext context, Map<String, dynamic> mapData) async {
    final response = await http.post(
      Uri.parse(SittingsApi.GET_VERIFICATION_CODE),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode(mapData),
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<int> forgetPassword(BuildContext context, int phone) async {
    final response = await http.post(
      Uri.parse(SittingsApi.FORGET_PASSWORD),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode({
        'mobile': phone,
      }),
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return jsonDecode(response.body)['code'];
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return 0;
  }

  Future<bool> resetPassword(
      BuildContext context, Map<String, dynamic> data) async {
    final response = await http.post(
      Uri.parse(SittingsApi.RESET_PASSWORD),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode(data),
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> changePassword(
      BuildContext context, Map<String, dynamic> data) async {
    final response = await http.post(
      Uri.parse(SittingsApi.CHANGE_PASSWORD),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode(data),
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> updateProfile(
      BuildContext context, Map<String, dynamic> data) async {
    final response = await http.post(
      Uri.parse(SittingsApi.UPDATE_PROFILE),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode
      },
      body: jsonEncode(data),
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> sendMessageToAdmin(
      BuildContext context, Map<String, dynamic> data) async {
    final response = await http.post(
      Uri.parse(SittingsApi.CONTACT_REQUESTS),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken
      },
      body: jsonEncode(data),
    );

    if (response.statusCode == 201) {
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }
}
