import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';

import '../sittings_api.dart';

class CardPaymentController extends GetxController {
  BuildContext context;

  RxList<CardPayment> listCards = <CardPayment>[].obs;
  RxBool isLoading = false.obs;

  static CardPaymentController get to => Get.find();

  CardPaymentController({required this.context});

  @override
  void onInit() {
    getCards();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    listCards.clear();
    super.onClose();
  }

  Future<List<CardPayment>> getCards() async {
    isLoading.value = true;
    var response = await http.get(
      Uri.parse(SittingsApi.CARD_PAYMENT),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
    );

    if (response.statusCode == 200) {
      var cardJson = jsonDecode(response.body)['list'] as List;
      listCards.value = cardJson
          .map((addressObjectJson) => CardPayment.fromJson(addressObjectJson))
          .toList();
      isLoading.value = false;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
      isLoading.value = false;
    }

    return listCards;
  }

  Future<bool> addPayment(BuildContext context, CardPayment cardPayment) async {
    final response = await http.post(
      Uri.parse(SittingsApi.CARD_PAYMENT),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
      body: jsonEncode(dataCardPaymentJson(cardPayment)),
    );

    if (response.statusCode == 201) {
      var allDataObject = jsonDecode(response.body)['object'];
      int id = allDataObject['id'];
      cardPayment.id = id;
      listCards.add(cardPayment);

      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> editAddress(
      BuildContext context, CardPayment cardPayment, int index) async {
    final response = await http.put(
      Uri.parse(SittingsApi.CARD_PAYMENT + '/' + cardPayment.id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
      body: jsonEncode(dataCardPaymentJson(cardPayment)),
    );

    if (response.statusCode == 200) {
      listCards.removeAt(index);
      listCards.insert(index, cardPayment);
      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    print(cardPayment.id.toString());
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Future<bool> deletePayment(BuildContext context, int idCardPayment) async {
    final response = await http.delete(
      Uri.parse(SittingsApi.CARD_PAYMENT + '/' + idCardPayment.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
        'lang': SharedPrefController().getLangCode,
      },
    );

    if (response.statusCode == 200) {
      listCards.removeWhere((item) => item.id == idCardPayment);

      Helpers.showSnackBar(
        context,
        message: jsonDecode(response.body)['message'],
      );
      return true;
    }
    Helpers.showSnackBar(
      context,
      message: jsonDecode(response.body)['message'],
      error: true,
    );
    return false;
  }

  Map<String, dynamic> dataCardPaymentJson(CardPayment cardPayment) {
    return {
      'holder_name': cardPayment.holderName,
      'card_number': cardPayment.cardNumber,
      'exp_date': cardPayment.expDate,
      'cvv': cardPayment.cvv,
      'type': cardPayment.type,
    };
  }
}
