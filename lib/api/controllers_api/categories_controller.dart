import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/model/category.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/model/product_details.dart';
import 'package:store_app/model/product_offer.dart';
import 'package:store_app/model/sub_category.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import '../sittings_api.dart';

class CategoriesController extends GetxController {
  RxList<Category> listCategory = <Category>[].obs;
  RxList<Product> listProductFavorite = <Product>[].obs;
  RxBool isLoading = false.obs;

  BuildContext context;

  static CategoriesController get to => Get.find();

  CategoriesController({required this.context});

  @override
  void onInit() {
    getCategories();
    getProductsFavorite();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    listCategory.clear();
    listProductFavorite.clear();
    super.onClose();
  }

  Future<List<Category>> getCategories() async {
    var response = await http.get(
      Uri.parse(SittingsApi.GET_ALL_CATEGORIES),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var categoriesJson = jsonDecode(response.body)['list'] as List;
      listCategory.value = categoriesJson
          .map((categoryObjectJson) => Category.fromJson(categoryObjectJson))
          .toList();
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return listCategory;
  }

  Future<List<Product>> getSubCategoryProducts(String id) async {
    List<Product> listProduct = [];
    var response = await http.get(
      Uri.parse(SittingsApi.GET_ALL_PRODUCT + '/' + id),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var productsJson = jsonDecode(response.body)['list'] as List;
      listProduct = productsJson
          .map((categoryObjectJson) => Product.fromJson(categoryObjectJson))
          .toList();
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return listProduct;
  }

  Future<List<Product>> getProducts({String type = 'latest_products'}) async {
    List<Product> listProduct = [];
    var response = await http.get(
      Uri.parse(SittingsApi.HOME),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var allData = jsonDecode(response.body)['data'];
      var productsJson = allData[type] as List;
      listProduct = productsJson
          .map((categoryObjectJson) => Product.fromJson(categoryObjectJson))
          .toList();

    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return listProduct;
  }

  Future<List<SubCategory>> getSubCategories(String id) async {
    List<SubCategory> listSubCategory = [];
    var response = await http.get(
      Uri.parse(SittingsApi.GET_ALL_CATEGORIES + '/' + id),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var subCategoriesJson = jsonDecode(response.body)['list'] as List;
      listSubCategory = subCategoriesJson
          .map((categoryObjectJson) => SubCategory.fromJson(categoryObjectJson))
          .toList();
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return listSubCategory;
  }

  Future<List<ProductDetails>> getProductDetails(String id) async {
    List<ProductDetails> listProductDetails = [];

    var response = await http.get(
      Uri.parse(SittingsApi.PRODUCT_DETAILS + '/' + id),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var objectData = jsonDecode(response.body)['object'];

      listProductDetails.add(ProductDetails.fromJson(objectData));
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return listProductDetails;
  }

  Future<List<Product>> getProductsFavorite() async {
    isLoading.value = true;
    var response = await http.get(
      Uri.parse(SittingsApi.PRODUCT_FAVORITE),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        'Accept': 'application/json',
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var listJson = jsonDecode(response.body)['list'] as List;
      listProductFavorite.value = listJson
          .map((categoryObjectJson) => Product.fromJson(categoryObjectJson))
          .toList();
      isLoading.value = false;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
      isLoading.value = false;
    }

    return listProductFavorite;
  }

  Future<List<ProductOffer>> getProductsOffer() async {
    List<ProductOffer> listProductsOffer = [];
    var response = await http.get(
      Uri.parse(SittingsApi.PRODUCT_OFFERS),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
    );

    if (response.statusCode == 200) {
      var listJson = jsonDecode(response.body)['list'] as List;
      listProductsOffer = listJson
          .map(
              (categoryObjectJson) => ProductOffer.fromJson(categoryObjectJson))
          .toList();
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return listProductsOffer;
  }

  Future<bool> changeFavoriteProduct(int idProduct) async {
    var response = await http.post(
      Uri.parse(SittingsApi.PRODUCT_FAVORITE),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
      body: jsonEncode({
        'product_id': idProduct.toString(),
      }),
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message']);

      return true;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }

    return false;
  }

  Future<bool> addRate(int idProduct, double rate) async {

    var response = await http.post(
      Uri.parse(SittingsApi.PRODUCT_RATE),
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'lang': SharedPrefController().getLangCode,
        HttpHeaders.authorizationHeader: SharedPrefController().getTokenType +
            ' ' +
            SharedPrefController().getToken,
      },
      body: {
        'product_id': jsonEncode(idProduct),
        'rate': jsonEncode(rate),
      },
    );

    if (response.statusCode == 200) {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message']);

      return true;
    } else {
      Helpers.showSnackBar(context,
          message: jsonDecode(response.body)['message'], error: true);
    }
    return false;
  }
}
