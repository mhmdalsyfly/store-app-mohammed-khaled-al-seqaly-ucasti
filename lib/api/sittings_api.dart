class SittingsApi {
  static final String BASE_URL = 'https://smart-store.mr-dev.tech/api/';
  static final String GET_CITIES = BASE_URL + 'cities';
  static final String GET_VERIFICATION_CODE = BASE_URL + 'auth/activate';
  static final String REGISTER = BASE_URL + 'auth/register';
  static final String LOGIN = BASE_URL + 'auth/login';
  static final String FORGET_PASSWORD = BASE_URL + 'auth/forget-password';
  static final String RESET_PASSWORD = BASE_URL + 'auth/reset-password';
  static final String CHANGE_PASSWORD = BASE_URL + 'auth/change-password';
  static final String UPDATE_PROFILE = BASE_URL + 'auth/update-profile';
  static final String CONTACT_REQUESTS = BASE_URL + 'contact-requests';
  static final String GET_ALL_CATEGORIES = BASE_URL + 'categories';
  static final String GET_ALL_PRODUCT = BASE_URL + 'sub-categories';
  static final String HOME = BASE_URL + 'home';
  static final String PRODUCT_DETAILS = BASE_URL + 'products';
  static final String PRODUCT_FAVORITE = BASE_URL + 'favorite-products';
  static final String PRODUCT_RATE = BASE_URL + 'products/rate';
  static final String PRODUCT_OFFERS = BASE_URL + 'offers';
  static final String ADDRESS = BASE_URL + 'addresses';
  static final String CARD_PAYMENT = BASE_URL + 'payment-cards';
  static final String ORDER = BASE_URL + 'orders';

}
