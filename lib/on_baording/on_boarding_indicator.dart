import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class OnBoardingIndicator extends StatelessWidget {
  bool selected;

  OnBoardingIndicator({
    required this.selected,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(10),
      width: selected ? SizeConfig.scaleWidth(36) : SizeConfig.scaleWidth(18),
      decoration: BoxDecoration(
        color: selected ? AppColors.BG_BUTTON : AppColors.BG_INDICATOR,
        borderRadius: BorderRadius.circular(7),
      ),
    );
  }
}
