import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class OnBoardingContent extends StatefulWidget {
  String image;

  OnBoardingContent({required this.image});

  @override
  _OnBoardingContentState createState() => _OnBoardingContentState();
}

class _OnBoardingContentState extends State<OnBoardingContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          widget.image,
          height: SizeConfig.scaleHeight(635),
          fit: BoxFit.cover,
          width: double.infinity,
        ),
      ],
    );
  }
}
