import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/model/content_on_boarding.dart';
import 'package:store_app/on_baording/on_baording_content.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container_on_boarding1.dart';
import 'package:store_app/widgets/my_container_on_boarding2.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PageViewScreen extends StatefulWidget {
  @override
  _PageViewScreenState createState() => _PageViewScreenState();
}

class _PageViewScreenState extends State<PageViewScreen> {
  late PageController _pageController;

  int _currentPage = 0;
  bool check = true;

  List<ContentOnBoarding> _list = [];

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void didChangeDependencies() {
    _list = [
      ContentOnBoarding(
        title: AppLocalizations.of(context)!.welcome,
        subTitle: AppLocalizations.of(context)!.welcomeToStore,
        image: 'images/on_boarding/on_boarding3.png',
      ),
      ContentOnBoarding(
        title: AppLocalizations.of(context)!.allProducts,
        subTitle: AppLocalizations.of(context)!.descStore,
        image: 'images/on_boarding/on_boarding2.png',
      ),
      ContentOnBoarding(
        title: AppLocalizations.of(context)!.deliciousProducts,
        subTitle:AppLocalizations.of(context)!.healthyProducts,
        image: 'images/on_boarding/on_boarding1.png',
      ),
    ];
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView.builder(
            itemCount: _list.length,
            controller: _pageController,
            physics: check ? null : new NeverScrollableScrollPhysics(),
            onPageChanged: (int currentPage) {
              setState(() {
                _currentPage = currentPage;
              });
              if (_currentPage < _list.length) {
                check = true;
              }
            },
            itemBuilder: (context, index) => OnBoardingContent(
              image: _list[index].image,
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: check
                ? MyContainerOnBoarding1(list: _list, currentPage: _currentPage)
                : MyContainerOnBoarding2(
                    onPressed: () {
                      /// TODO:: NAVIGATOR TO LOGIN SCREEN
                      Get.offAllNamed('/sign_in_screen');
                    },
                  ),
          ),
          Visibility(
            visible: check,
            child: Positioned.directional(
              textDirection: TextDirection.ltr,
              top: SizeConfig.scaleHeight(70),
              end: SizeConfig.scaleWidth(0),
              child: MyTextButton(
                title: AppLocalizations.of(context)!.skip,
                fontSize: 18,
                fontWeight: FontWeight.w400,
                colorText: AppColors.WHITE,
                bgColor: AppColors.TRANSPARENT,
                onPressed: () {
                  _pageController.nextPage(
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeIn,
                  );
                  if (_currentPage == _list.length - 1) {
                    setState(() {
                      check = false;
                    });
                    print('This is final Skip');
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/**
 *
 * TextButton(
    onPressed: () {
    _pageController.nextPage(
    duration: Duration(milliseconds: 500),
    curve: Curves.easeIn,
    );
    if (_currentPage == _list.length - 1) {
    setState(() {
    check = false;
    });
    print('This is final Skip');
    }
    },
    child: MyText(
    title: 'Skip',
    fontSize: 18,
    color: AppColors.WHITE,
    ),
    )
 * */
