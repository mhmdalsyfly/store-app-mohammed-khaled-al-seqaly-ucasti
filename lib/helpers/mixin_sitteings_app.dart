import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:store_app/model/item_profile.dart';
import 'package:store_app/model/order_product.dart';

mixin MixinSittingsApp {


  Future<void> loginFacebook() async {
    try {
      // by default the login method has the next permissions ['email','public_profile']
      LoginResult accessToken = (await FacebookAuth.instance.login());
      print('${accessToken.accessToken!.toJson()} all data');
      // get the user data
      final userData = await FacebookAuth.instance.getUserData();
      print('$userData data user');
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> loginGoogle() async {
    GoogleSignIn googleSignIn = GoogleSignIn(
      scopes: [
        'email',
        'https://www.googleapis.com/auth/contacts.readonly',
      ],
    );
    try {
      await googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }
}
