import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/widgets/my_text.dart';

class Helpers {
  static showSnackBar(
    BuildContext context, {
    required String message,
    bool error = false,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: MyText(
          title: message,
          fontWeight: FontWeight.w500,
          fontSize: 16,
          color: AppColors.WHITE,
        ),
        behavior: SnackBarBehavior.floating,
        elevation: 4,
        backgroundColor: error ? AppColors.RED : AppColors.GREEN,
      ),
    );
  }
}
