import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:store_app/on_baording/page_view_screen.dart';
import 'package:store_app/screens/other_screen/contact_us_screen.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/storage/sqflite/db_provider.dart';
import 'package:store_app/screens/screens.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//  android and ios
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up :  ${message.messageId}');
}

// channel android
const AndroidNotificationChannel channel = AndroidNotificationChannel(
    '1', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
    playSound: true);

 //  Local Notifications
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefController().initSharedPref();
  await DBProvider().initDatabase();
  await Firebase.initializeApp();
  // android && ios
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  // android
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  //ios
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/launch_screen',
      locale: Locale(SharedPrefController().getLangCode),
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en', ''),
        Locale('ar', ''),
      ],
      getPages: <GetPage>[
        GetPage(name: '/launch_screen', page: () => LaunchScreen()),
        GetPage(name: '/page_view_screen', page: () => PageViewScreen()),

        /// Auth
        GetPage(name: '/sign_in_screen', page: () => SignInScreen()),
        GetPage(name: '/sign_up_screen', page: () => SignUpScreen()),
        GetPage(
            name: '/forgot_password_screen',
            page: () => ForgotPasswordScreen()),
        GetPage(name: '/verification_screen', page: () => VerificationScreen()),
        GetPage(
            name: '/change_password_screen',
            page: () => ChangePasswordScreen()),
        GetPage(
            name: '/update_profile_screen', page: () => UpdateProfileScreen()),

        /// Home
        GetPage(
            name: '/main_bottom_navigation_screen',
            page: () => MainBottomNavigationScreen()),
        GetPage(
            name: '/products_offer_screen', page: () => ProductsOfferScreen()),

        /// Address
        GetPage(name: '/address_screen', page: () => AddressScreen()),
        GetPage(
            name: '/chose_address_screen', page: () => ChoseAddressScreen()),
        GetPage(name: '/add_address_screen', page: () => AddAddressScreen()),

        /// Payment And Cards
        GetPage(name: '/payment_card_screen', page: () => PaymentCardsScreen()),
        GetPage(
            name: '/add_payment_card_screen',
            page: () => AddPaymentCardScreen()),
        GetPage(name: '/cart_screen', page: () => CartScreen()),
        GetPage(name: '/cart_submit_screen', page: () => CartSubmitScreen()),

        ///Order
        GetPage(name: '/order_screen', page: () => OrderScreen()),
        GetPage(
            name: '/order_details_screen', page: () => OrderDetailsScreen()),

        GetPage(name: '/language_screen', page: () => LanguageScreen()),
        GetPage(name: '/contact_us_screen', page: () => ContactUsScreen()),
        GetPage(name: '/about_screen', page: () => AboutScreen()),
        GetPage(
            name: '/privacy_policy_screen', page: () => PrivacyPolicyScreen()),
        GetPage(
            name: '/categories_screen', page: () => CategoriesScreen()),
      ],
    );
  }
}
