import 'package:store_app/model/city.dart';

class Address {
  late int id;
  late String name;
  late String info;
  late int contactNumber;
  late int cityId;
  late City? city;

  Address({
    required this.id,
    required this.name,
    required this.info,
    required this.contactNumber,
    required this.cityId,
    required this.city,
  });

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    info = json['info'];
    contactNumber = int.parse(json['contact_number'].toString());
    cityId = json['city_id'];
    city = json['city'] != null ?  City.fromJson(json['city']) : null;
  }

  Address.fromJsonWithOutCity(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    info = json['info'];
    contactNumber = json['contact_number'];
    cityId = json['city_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['info'] = this.info;
    data['contact_number'] = this.contactNumber;
    data['city_id'] = this.cityId;
    return data;
  }
}
