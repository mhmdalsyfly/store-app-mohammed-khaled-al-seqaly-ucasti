class User {
  late int id;
  late int cityId;
  late String name;
  late String mobile;
  late String password;
  late String gender;
  late String storeApiKey;

  User({
    required this.id,
    required this.cityId,
    required this.name,
    required this.mobile,
    required this.password,
    required this.gender,
    required this.storeApiKey,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cityId = json['city_id'];
    name = json['name'];
    mobile = json['mobile'];
    password = json['password'];
    gender = json['gender'];
    storeApiKey = json['STORE_API_KEY'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city_id'] = this.cityId;
    data['name'] = this.name;
    data['mobile'] = this.mobile;
    data['password'] = this.password;
    data['gender'] = this.gender;
    data['STORE_API_KEY'] = this.storeApiKey;
    return data;
  }

}
