class OrderProduct {
  late String image;
  late String nameEn;
  late String nameAr;
  late int quantity;

  OrderProduct({
    required this.image,
    required this.nameEn,
    required this.nameAr,
    required this.quantity,
  });

  OrderProduct.fromJson(Map<String, dynamic> map) {
    image = map['image_url'];
    nameEn = map['name_en'];
    nameAr = map['name_ar'];
    quantity = map['order_quantity'];
  }
}
