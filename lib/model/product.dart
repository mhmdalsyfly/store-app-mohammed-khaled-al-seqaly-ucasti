class Product {
  late int id;
  late String nameEn;
  late String nameAr;
  late String imageUrl;
  late num price;
  late int quantity;
  late num overalRate;
  late bool isFavorite;
  int? offerPrice;

  Product({
    required this.id,
    required this.quantity,
    required this.nameEn,
    required this.nameAr,
    required this.imageUrl,
    required this.price,
    required this.overalRate,
    required this.isFavorite,
    this.offerPrice,
  });

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    price = json['price'];
    quantity = json['quantity'];
    overalRate = json['overal_rate'];
    imageUrl = json['image_url'];
    isFavorite = json['is_favorite'];
    offerPrice = json['offer_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_en'] = this.nameEn;
    data['name_ar'] = this.nameAr;
    data['price'] = this.price;
    data['overal_rate'] = this.overalRate;
    data['image_url'] = this.imageUrl;
    return data;
  }
}
