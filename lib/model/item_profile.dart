import 'package:flutter/cupertino.dart';

class ItemProfile {
  IconData leading;
  IconData trailing;
  String title;

  ItemProfile({
    required this.leading,
    required this.trailing,
    required this.title,
  });
}
