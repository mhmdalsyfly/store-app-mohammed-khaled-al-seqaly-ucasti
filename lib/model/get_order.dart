class GetOrder {
  late int id;
  late num total;
  late String date;
  late String paymentType;
  late String status;
  late int storeId;
  late int userId;
  late int addressId;
  late int? paymentCardId;
  late int orderProductsCount;

  GetOrder({
   required this.id,
   required this.total,
   required this.date,
   required this.paymentType,
   required this.status,
   required this.storeId,
   required this.userId,
   required this.addressId,
   this.paymentCardId,
   required this.orderProductsCount,
  });

  GetOrder.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    total = json['total'];
    date = json['date'];
    paymentType = json['payment_type'];
    status = json['status'];
    storeId = json['store_id'];
    userId = json['user_id'];
    addressId = json['address_id'];
    paymentCardId = json['payment_card_id'];
    orderProductsCount = json['order_products_count'];
  }

}
