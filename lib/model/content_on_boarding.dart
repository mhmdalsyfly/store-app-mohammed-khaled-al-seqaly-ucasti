class ContentOnBoarding {
  late String title;
  late String subTitle;
  late String image;

  ContentOnBoarding({
    required this.title,
    required this.subTitle,
    required this.image,
  });
}
