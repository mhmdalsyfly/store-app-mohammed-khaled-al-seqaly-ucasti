class Cart {
  int? idCart;
  late int idProduct;
  late String image;
  late String nameEn;
  late String nameAr;
  late int quantity;
  late int requiredQuantity;
  late num price;
  int? idUser;

  Cart({
    this.idUser,
    required this.idProduct,
    required this.image,
    required this.nameEn,
    required this.nameAr,
    required this.quantity,
    required this.requiredQuantity,
    required this.price,
  });

  Cart.fromJson(Map<String, dynamic> map) {
    idCart = map['idCart'];
    idProduct = map['idProduct'];
    image = map['image'];
    nameEn = map['nameEn'];
    nameAr = map['nameAr'];
    quantity = map['quantity'];
    requiredQuantity = map['requiredQuantity'];
    price = map['price'];
    idUser = map['idUser'];
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['idProduct'] = idProduct;
    map['idUser'] = idUser;
    map['image'] = image;
    map['nameEn'] = nameEn;
    map['nameAr'] = nameAr;
    map['quantity'] = quantity;
    map['requiredQuantity'] = requiredQuantity;
    map['price'] = price;
    return map;
  }
}
