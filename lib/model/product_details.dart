class ProductDetails {
  late int id;
  late String nameEn;
  late String nameAr;
  late String infoEn;
  late String infoAr;
  late num price;
  late int quantity;
  late num overalRate;
  late int subCategoryId;
  late num productRate;
  late bool isFavorite;
  late String imageUrl;
  late List<String> images;
  late SubCategory1? subCategory;
  num? offerPrice;

  ProductDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    infoEn = json['info_en'];
    infoAr = json['info_ar'];
    price = json['price'];
    quantity = json['quantity'];
    overalRate = json['overal_rate'];
    subCategoryId = json['sub_category_id'];
    productRate = json['product_rate'];
    offerPrice = json['offer_price'];
    isFavorite = json['is_favorite'];
    imageUrl = json['image_url'];
    if (json['images'] != null) {
      images = [];
      json['images'].forEach((v) {
        images.add(v['image_url']);
      });
    }
    subCategory = json['sub_category'] != null
        ? SubCategory1.fromJson(json['sub_category'])
        : null;
  }

}

class SubCategory1 {
  late int id;
  late String nameEn;
  late String nameAr;
  late int categoryId;
  late String image;
  late String imageUrl;

  SubCategory1.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['name_en'];
    nameAr = json['name_ar'];
    categoryId = json['category_id'];
    image = json['image'];
    imageUrl = json['image_url'];
  }

}
