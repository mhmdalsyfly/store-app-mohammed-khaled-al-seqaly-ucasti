import 'package:flutter/material.dart';

class TabLayout {
  Tab tab;
  Widget widget;

  TabLayout({required this.tab, required this.widget});
}
