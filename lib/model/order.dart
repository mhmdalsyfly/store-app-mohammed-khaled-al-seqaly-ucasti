import 'dart:convert';

import 'package:store_app/model/send_cart.dart';

class Order {
  late String paymentType;
  late int addressId;
  late int cardId;
  late List<SendCart> cart;

  Order({
    required this.paymentType,
    required this.addressId,
    required this.cardId,
    required this.cart,
  });

  Order.fromJson(Map<String, dynamic> json) {
    paymentType = json['payment_type'];
    addressId = json['address_id'];
    cardId = json['card_id'];
    if (json['cart'] != null) {
      cart = <SendCart>[];
      json['cart'].forEach((v) {
        cart.add(SendCart.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['payment_type'] = this.paymentType.toString();
    data['address_id'] = this.addressId.toString();
    if (this.cardId != 0)
      data['card_id'] = this.cardId.toString();
    if (this.cart.isNotEmpty) {
      data['cart'] = jsonEncode(this.cart.map((v) => v.toJson()).toList());
    }
    return data;
  }
}
