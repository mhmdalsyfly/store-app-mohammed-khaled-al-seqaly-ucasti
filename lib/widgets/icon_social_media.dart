import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

class IconSocialMedia extends StatelessWidget {
  String icon;
  Color bgColor;
  GestureTapCallback onTap;

  IconSocialMedia({
    required this.icon,
    required this.bgColor,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        borderRadius: BorderRadius.circular(4),
        color: bgColor,
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(4),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: SizeConfig.scaleHeight(12)),
            height: SizeConfig.scaleHeight(50),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
            ),
            child: Image.asset(
              icon,
              height: SizeConfig.scaleHeight(24),
            ),
          ),
        ),
      ),
    );
  }
}
