import 'package:flutter/material.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class IconFavorite extends StatefulWidget {
  double width;
  double height;
  bool isFavorite;
  bool onClicked;
  Product product;

  IconFavorite({
    required this.isFavorite,
    required this.product,
    this.onClicked = true,
    this.height = 24,
    this.width = 24,
  });

  @override
  _IconFavoriteState createState() => _IconFavoriteState();
}

class _IconFavoriteState extends State<IconFavorite> {
  late bool click;
  bool changed = false;

  @override
  void initState() {
    click = widget.isFavorite;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onClicked
          ? () async {
              setState(() {
                changed = true;
              });
              changed = await CategoriesController(context: context)
                  .changeFavoriteProduct(widget.product.id);
              if (changed) {
                if (click) {
                  CategoriesController.to.listProductFavorite.removeWhere(
                    (element) => element.id == widget.product.id,
                  );

                  click = false;
                } else {
                  widget.product.isFavorite = true;
                  CategoriesController.to.listProductFavorite
                      .add(widget.product);
                  click = true;
                }

                setState(() {
                  changed = false;
                });
              } else {
                setState(() {
                  changed = false;
                });
              }
            }
          : null,
      child: changed
          ? Center(
              child: SizedBox(
                width: SizeConfig.scaleWidth(widget.width - 8),
                height: SizeConfig.scaleHeight(widget.height - 8),
                child: CircularProgressIndicator(
                  strokeWidth: 3,
                  valueColor:
                      AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                ),
              ),
            )
          : Container(
              width: SizeConfig.scaleWidth(widget.width),
              height: SizeConfig.scaleHeight(widget.height),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.WHITE,
              ),
              child: Icon(
                click ? Icons.favorite : Icons.favorite_border,
                color: AppColors.RED,
                size: 18,
              ),
            ),
    );
  }
}
