import 'package:flutter/material.dart';
import 'package:store_app/model/content_on_boarding.dart';
import 'package:store_app/on_baording/on_boarding_indicator.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container.dart';
import 'my_text.dart';

class MyContainerOnBoarding1 extends StatelessWidget {
  const MyContainerOnBoarding1({
    Key? key,
    required List<ContentOnBoarding> list,
    required int currentPage,
  }) : _list = list, _currentPage = currentPage, super(key: key);

  final List<ContentOnBoarding> _list;
  final int _currentPage;

  @override
  Widget build(BuildContext context) {
    return MyContainer(widget: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        MyText(
          title: _list[_currentPage].title,
          color: AppColors.TEXT_Bold,
          fontWeight: FontWeight.w600,
          fontSize: 30,
        ),
        SizedBox(height: SizeConfig.scaleHeight(20)),
        MyText(
          title: _list[_currentPage].subTitle,
          color: AppColors.TEXT_SMALL,
          fontSize: 18,
        ),
        Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OnBoardingIndicator(selected: _currentPage == 0),
            SizedBox(width: SizeConfig.scaleWidth(15)),
            OnBoardingIndicator(selected: _currentPage == 1),
            SizedBox(width: SizeConfig.scaleWidth(15)),
            OnBoardingIndicator(selected: _currentPage == 2),
          ],
        ),
        SizedBox(height: SizeConfig.scaleHeight(60))
      ],
    ),height: 265,);
  }
}
