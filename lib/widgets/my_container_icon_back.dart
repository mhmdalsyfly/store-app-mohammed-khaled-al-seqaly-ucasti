import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MyContainerIconBack extends StatelessWidget {
  GestureTapCallback onTap;
  Color color;

  MyContainerIconBack({required this.onTap, this.color = AppColors.WHITE});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.centerStart,
      child: Material(
        color: color,
        borderRadius: BorderRadius.circular(4),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(4),
          child: Container(
            width: SizeConfig.scaleWidth(32),
            height: SizeConfig.scaleHeight(32),
            alignment: Alignment.center,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(4)),
            child: Icon(
              Icons.chevron_left,
              color: AppColors.ICON_BACK,
              size: 20,
            ),
          ),
        ),
      ),
    );
  }
}
