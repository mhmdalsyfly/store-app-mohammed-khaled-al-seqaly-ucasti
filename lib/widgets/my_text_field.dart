import 'package:flutter/material.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MyTextField extends StatelessWidget {
  TextInputType keyboardType;
  String label;
  bool obscure;
  bool maxMinLine;
  TextEditingController? controller;

  MyTextField({
    required this.keyboardType,
    required this.label,
    required this.obscure,
    this.controller,
    this.maxMinLine = false,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: keyboardType,
      cursorColor: AppColors.TEXT_Bold,
      obscureText: obscure,
      textDirection: SharedPrefController().getLangCode == 'en'
          ? TextDirection.rtl
          : TextDirection.ltr,
      textAlign: SharedPrefController().getLangCode == 'en'
          ? TextAlign.left
          : TextAlign.right,
      minLines: maxMinLine ? 3 : 1,
      maxLines: maxMinLine ? 4 : 1,
      decoration: InputDecoration(
        hintTextDirection: SharedPrefController().getLangCode == 'en'
            ? TextDirection.rtl
            : TextDirection.ltr,
        labelText: label,
        filled: true,
        fillColor: AppColors.WHITE,
        labelStyle: TextStyle(
          color: AppColors.LABEL_HINT,
          fontSize: SizeConfig.scaleTextFont(16),
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: AppColors.BORDER_TEXT_FIELD,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: AppColors.BORDER_TEXT_FIELD,
          ),
        ),
      ),
    );
  }
}
