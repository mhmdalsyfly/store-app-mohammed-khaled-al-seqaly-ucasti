import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MyText extends StatelessWidget {
  String title;
  Color color;
  double fontSize;
  double height;
  FontWeight fontWeight;
  TextAlign textAlign;
  bool lineThrough;

  MyText({
    required this.title,
    this.color = AppColors.TEXT_SMALL,
    this.fontSize = 16,
    this.height = 1.3,
    this.fontWeight = FontWeight.w400,
    this.textAlign = TextAlign.center,
    this.lineThrough = false,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: textAlign,
      style: TextStyle(
        color: color,
        fontSize: SizeConfig.scaleTextFont(fontSize),
        fontWeight: fontWeight,
        height: height,
        decorationThickness: 2,
        decoration: lineThrough ? TextDecoration.lineThrough : null,
      ),
    );
  }
}
