import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/helpers/show_snakbar.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'icon_increase_and_decrease.dart';
import 'my_contianer_shape.dart';
import 'my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ItemListViewMyCart extends StatelessWidget {
  int index;

  ItemListViewMyCart({required this.index});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MyContainerShape(
          alignment: AlignmentDirectional.center,
          width: 90,
          height: SizeConfig.scaleHeight(90),
          bgContainer: AppColors.BG_ITEM_PRODUCT,
          paddingVertical: 10,
          paddingHorizontal: 10,
          enableRadius: false,
          enableShadow: false,
          boxShape: BoxShape.circle,
          child: ProgressiveImage(
            placeholder: AssetImage('images/image.png'),
            // size: 1.87KB
            thumbnail:
                NetworkImage(CartControllerGetx.to.listProducts[index].image),
            // size: 1.29MB
            image:
                NetworkImage(CartControllerGetx.to.listProducts[index].image),
            height: SizeConfig.scaleHeight(80),
            width: SizeConfig.scaleWidth(60),
          ),
        ),
        SizedBox(width: SizeConfig.scaleWidth(8)),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: SizeConfig.scaleHeight(8)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: SharedPrefController().getLangCode == 'en'
                        ? CartControllerGetx.to.listProducts[index].nameEn
                        : CartControllerGetx.to.listProducts[index].nameAr,
                    fontSize: 14,
                    color: AppColors.TEXT_Bold,
                    height: 1,
                    textAlign: TextAlign.start,
                  ),
                  GestureDetector(
                    onTap: () {
                      CartControllerGetx.to.deleteProduct(
                          CartControllerGetx.to.listProducts[index], index);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.scaleWidth(4),
                        vertical: SizeConfig.scaleHeight(4),
                      ),
                      child: Icon(
                        Icons.close,
                        size: 16,
                        color: AppColors.LABEL_HINT,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(8)),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MyText(
                    title: AppLocalizations.of(context)!.quantity1,
                    textAlign: TextAlign.start,
                    fontSize: 14,
                    height: 1,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(8)),
                  Obx(() {
                    return MyText(
                      title: CartControllerGetx
                          .to.listProducts[index].requiredQuantity
                          .toString(),
                      color: AppColors.TEXT_Bold,
                    );
                  }),
                  SizedBox(width: SizeConfig.scaleWidth(16)),
                  MyText(
                    title: AppLocalizations.of(context)!.unitPrice,
                    textAlign: TextAlign.start,
                    fontSize: 14,
                    height: 1,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(8)),
                  MyText(
                    title: CartControllerGetx.to.listProducts[index].price
                        .toString(),
                    color: AppColors.TEXT_Bold,
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(8)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      MyText(
                        title: AppLocalizations.of(context)!.totalPrice,
                        textAlign: TextAlign.start,
                        fontSize: 14,
                        height: 1,
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(8)),
                      Obx(() {
                        return MyText(
                          title:
                              '${CartControllerGetx.to.listProducts[index].price * CartControllerGetx.to.listProducts[index].requiredQuantity}',
                          height: 1,
                          color: AppColors.SELECTED_ITEM,
                          fontWeight: FontWeight.w500,
                        );
                      }),
                    ],
                  ),
                  MyContainerShape(
                    height: SizeConfig.scaleHeight(34),
                    width: 104,
                    paddingHorizontal: 0,
                    paddingVertical: 0,
                    enableShadow: false,
                    borderRadius: 6,
                    bgContainer: AppColors.WHITE,
                    enableBorder: true,
                    colorBorder: AppColors.BORDER,
                    widthBorder: 1,
                    child: Row(
                      children: [
                        IconIncreaseAndDecrease(
                          icon: Icons.add,
                          onTap: () {
                            if (CartControllerGetx
                                    .to.listProducts[index].requiredQuantity >=
                                CartControllerGetx
                                    .to.listProducts[index].quantity) {
                              Helpers.showSnackBar(
                                context,
                                message:AppLocalizations.of(context)!.orderedQuantity,
                                error: true,
                              );
                            } else {
                              CartControllerGetx.to.addQuantity(index);
                            }
                          },
                        ),
                        VerticalDivider(
                          width: 1,
                          color: AppColors.BORDER,
                        ),
                        Expanded(
                          flex: 2,
                          child: Obx(() {
                            return MyText(
                              title:
                                  '${CartControllerGetx.to.listProducts[index].requiredQuantity} Q',
                              color: AppColors.TEXT_Bold,
                              fontSize: 14,
                            );
                          }),
                        ),
                        VerticalDivider(
                          width: 1,
                          color: AppColors.BORDER,
                        ),
                        IconIncreaseAndDecrease(
                          icon: Icons.remove,
                          onTap: () {
                            if (CartControllerGetx
                                    .to.listProducts[index].requiredQuantity >
                                1) {
                              CartControllerGetx.to.decreaseQuantity(index);
                            }
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
