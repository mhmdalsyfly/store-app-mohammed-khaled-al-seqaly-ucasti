import 'package:flutter/material.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'icon_favorit.dart';
import 'my_rating_bar.dart';
import 'my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ItemGridViewProduct extends StatelessWidget {
  Product product;
  GestureTapCallback onTap;
  double? height;
  double? width;
  int offer;

  ItemGridViewProduct({
    required this.product,
    required this.onTap,
    required this.offer,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(5),
          vertical: SizeConfig.scaleHeight(5),
        ),
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.scaleHeight(100),
              child: Stack(
                children: [
                  Container(
                    height: SizeConfig.scaleHeight(100),
                    width: double.infinity,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: AppColors.BG_ITEM_PRODUCT,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    // child: Image.network(product.imageUrl),
                    child: ProgressiveImage(
                      placeholder: AssetImage('images/image.png'),
                      // size: 1.87KB
                      thumbnail: NetworkImage(product.imageUrl),
                      // size: 1.29MB
                      image: NetworkImage(product.imageUrl),
                      height: SizeConfig.scaleHeight(90),
                      fit: BoxFit.contain,
                      width: SizeConfig.scaleWidth(80),
                    ),
                  ),
                  Positioned(
                    top: SizeConfig.scaleHeight(4),
                    right: SizeConfig.scaleWidth(8),
                    child: IconFavorite(
                      isFavorite: product.isFavorite,
                      product: product,
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Visibility(
                      visible: offer > 0,
                      child: Image.asset(
                        'images/banner_offer.png',
                        width: SizeConfig.scaleWidth(50),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(6)),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(5),
                ),
                child: MyText(
                  title: SharedPrefController().getLangCode == 'en'
                      ? product.nameEn
                      : product.nameAr,
                  color: AppColors.TEXT_Bold,
                  fontSize: 14,
                  textAlign: TextAlign.start,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(2),
              ),
              child: Align(
                alignment: AlignmentDirectional.centerStart,
                child: MyRatingBar(
                  initialRating: double.parse(product.overalRate.toString()),
                  ignoreGestures: true,
                  function: (rating) {
                    print(rating);
                  },
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(5),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    MyText(
                      title: AppLocalizations.of(context)!.price,
                      fontSize: 14,
                    ),
                    Visibility(
                      visible: product.offerPrice != null,
                      child: MyText(
                        title: '${product.price} ₪',
                        color: AppColors.GREEN,
                        fontSize: 15,
                        textAlign: TextAlign.start,
                        fontWeight: FontWeight.w500,
                        lineThrough: product.offerPrice != null,
                      ),
                    ),
                    Visibility(
                        visible: product.offerPrice != null,
                        child: SizedBox(width: SizeConfig.scaleWidth(4))),
                    MyText(
                      title:
                          '${product.offerPrice == null ? product.price : product.offerPrice} ₪',
                      color: AppColors.GREEN,
                      fontSize: 15,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
