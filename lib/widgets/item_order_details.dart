import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/model/order_product.dart';
import 'package:store_app/screens/cart/add_payment_card_screen.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_contianer_shape.dart';
import 'my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ItemOrderDetails extends StatelessWidget {
  final OrderProduct orderProduct;

  ItemOrderDetails({required this.orderProduct});

  @override
  Widget build(BuildContext context) {
    return MyContainerShape(
      paddingVertical: 15,
      paddingHorizontal: 15,
      borderRadius: 8,
      width: double.infinity,
      bgContainer: AppColors.WHITE,
      enableShadow: false,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.network(
            orderProduct.image,
            width: SizeConfig.scaleWidth(60),
            height: SizeConfig.scaleHeight(60),
            fit: BoxFit.contain,
          ),
          SizedBox(width: SizeConfig.scaleWidth(20)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title: SharedPrefController().getLangCode == 'en'
                    ? orderProduct.nameEn
                    : orderProduct.nameAr,
                fontSize: 18,
                color: AppColors.TEXT_Bold,
                textAlign: TextAlign.start,
              ),
              SizedBox(height: SizeConfig.scaleHeight(8)),
              Row(
                children: [
                  MyText(
                    title: AppLocalizations.of(context)!.quantity,
                    fontSize: 16,
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(4)),
                  MyText(
                    title: orderProduct.quantity.toString(),
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    textAlign: TextAlign.start,
                  ),
                ],
              )
            ],
          ),
          Spacer(),
        ],
      ),
    );
  }
}
