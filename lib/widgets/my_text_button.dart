import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_text.dart';

class MyTextButton extends StatelessWidget {
  VoidCallback? onPressed;
  String title;
  Color bgColor;
  Color colorText;
  double fontSize;
  FontWeight fontWeight;
  bool enabledSize;
  double height;
  double width;
  double radius;

  MyTextButton({
    this.onPressed,
    required this.title,
    required this.bgColor,
    required this.colorText,
    required this.fontSize,
    required this.fontWeight,
    this.enabledSize = false,
    this.radius = 8,
    this.height = 0,
    this.width = double.infinity,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: TextButton.styleFrom(
        backgroundColor: bgColor,
        minimumSize: enabledSize
            ? Size(
                SizeConfig.scaleWidth(width),
                SizeConfig.scaleHeight(height),
              )
            : null,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
      ),
      child: MyText(
        title: title,
        color: colorText,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
    );
  }
}
