import 'package:flutter/material.dart';
import 'package:store_app/model/address.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_text.dart';
import 'my_text_button.dart';

class ItemAddress2 extends StatelessWidget {
  GestureTapCallback onTap;
  Address address;

  ItemAddress2({required this.onTap, required this.address});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(16),
          vertical: SizeConfig.scaleHeight(16),
        ),
        margin: EdgeInsets.symmetric(
          vertical: SizeConfig.scaleHeight(10),
        ),
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            SizedBox(
              width: double.infinity,
              child: MyText(
                title: address.name +
                    '  -  ' +
                    '${address.city == null ? '' : SharedPrefController().getLangCode == 'en' ? address.city!.nameEn : address.city!.nameAr}',
                color: AppColors.TEXT_Bold,
                fontSize: 14,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(10),
            ),
            MyText(
              title: address.info,
              color: AppColors.TEXT_SMALL,
              fontSize: 16,
              textAlign: TextAlign.start,
            ),
          ],
        ),
      ),
    );
  }
}
