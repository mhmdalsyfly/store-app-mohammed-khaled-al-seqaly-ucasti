import 'package:flutter/material.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';

class MyTextField extends StatelessWidget {
  TextInputType textInputType;
  TextEditingController? controller;
  String hint;
  Color borderColor;

  MyTextField({
    required this.textInputType,
    required this.hint,
    this.controller,
    this.borderColor =AppColors.BG_BUTTON_DELETE,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: textInputType,
      controller: controller,
      cursorColor: AppColors.TEXT_Bold,
      style: TextStyle(color: AppColors.TEXT_Bold),
      textDirection: SharedPrefController().getLangCode == 'en'
          ? TextDirection.rtl
          : TextDirection.ltr,
      textAlign: SharedPrefController().getLangCode == 'en'
          ? TextAlign.left
          : TextAlign.right,
      decoration: InputDecoration(
        hintTextDirection: SharedPrefController().getLangCode == 'en'
            ? TextDirection.rtl
            : TextDirection.ltr,
        hintText: hint,
        hintStyle: TextStyle(color: AppColors.LABEL_HINT.withOpacity(.5)),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: borderColor,
          ),
          borderRadius: BorderRadius.circular(4),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: borderColor,
          ),
          borderRadius: BorderRadius.circular(4),
        ),
      ),
    );
  }
}

