import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';

class IconIncreaseAndDecrease extends StatelessWidget {
  GestureTapCallback onTap;
  IconData icon;

  IconIncreaseAndDecrease({required this.onTap, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        color: AppColors.WHITE,
        borderRadius: BorderRadius.circular(6),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(6),
          child: Container(
            width: double.infinity,
            height: double.infinity,
            child: Icon(
              icon,
              color: AppColors.TEXT_Bold,
              size: 16,
            ),
          ),
        ),
      ),
    );
  }
}

