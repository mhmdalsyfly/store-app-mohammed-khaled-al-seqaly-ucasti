import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MyContainer extends StatelessWidget {
  double paddingTop;
  double paddingStart;
  double paddingEnd;
  double paddingBottom;
  double height;
  Widget widget;

  MyContainer({
    this.paddingTop = 34,
    this.paddingStart = 25,
    this.paddingEnd = 25,
    this.paddingBottom = 0,
    this.height = 400,
    required this.widget,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        padding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(paddingTop),
            start: SizeConfig.scaleWidth(paddingStart),
            end: SizeConfig.scaleWidth(paddingEnd),
            bottom: SizeConfig.scaleHeight(paddingBottom)),
        height: SizeConfig.scaleHeight(height),
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          borderRadius: BorderRadiusDirectional.only(
            topEnd: Radius.circular(60),
          ),
        ),
        child: widget);
  }
}
