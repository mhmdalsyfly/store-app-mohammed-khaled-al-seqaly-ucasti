import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:store_app/utils/app_colors.dart';

class MyRatingBar extends StatelessWidget {
  double initialRating;
  bool ignoreGestures;
  Function function;

  MyRatingBar({
    required this.initialRating,
    required this.ignoreGestures,
    required this.function,
  });

  @override
  Widget build(BuildContext context) {
    return RatingBar.builder(
      initialRating: initialRating,
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 20,
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
      ),
      tapOnlyMode: false,
      glowColor: AppColors.TRANSPARENT,
      ignoreGestures: ignoreGestures,
      onRatingUpdate: (double rating) {
        function(rating);
      },
    );
  }
}

