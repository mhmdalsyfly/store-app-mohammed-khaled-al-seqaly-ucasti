import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_container.dart';
import 'package:store_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'my_text.dart';

class MyContainerOnBoarding2 extends StatelessWidget {
  VoidCallback? onPressed;

  MyContainerOnBoarding2({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return MyContainer(
      widget: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: SizeConfig.scaleHeight(21)),
          Image.asset(
            'images/logo2.png',
            height: SizeConfig.scaleHeight(143),
            width: SizeConfig.scaleWidth(290),
          ),
          Spacer(),
          MyTextButton(
            title: AppLocalizations.of(context)!.startNow,
            bgColor: AppColors.BG_BUTTON,
            colorText: AppColors.WHITE,
            fontSize: 18,
            fontWeight: FontWeight.w600,
            enabledSize: true,
            height: 55,
            onPressed: onPressed,
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(69),
          )
        ],
      ),
    );
  }
}
/**
 *
 *
    TextButton(
    onPressed: onPressed,
    child: MyText(
    title: '',
    color: AppColors.WHITE,
    fontSize: 18,
    fontWeight: FontWeight.w600,
    ),
    style: TextButton.styleFrom(
    backgroundColor: AppColors.BG_BUTTON,
    minimumSize: Size(
    double.infinity,
    SizeConfig.scaleHeight(55),
    ),
    shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(8),
    ),
    ),
    )
 */
