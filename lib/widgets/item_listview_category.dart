import 'package:flutter/material.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:store_app/model/category.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_text.dart';

class ItemListViewCategory extends StatelessWidget {
  Category category;
  GestureTapCallback onTap;
  double width;
  double height;
  double fontTitle;
  FontWeight fontWeight;

  ItemListViewCategory({
    required this.category,
    required this.onTap,
    this.fontTitle = 16,
    this.height = 80,
    this.width = 64,
    this.fontWeight = FontWeight.w400,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.WHITE,
      borderRadius: BorderRadius.circular(8),
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(8),
        child: Container(
          width: SizeConfig.scaleWidth(100),
          height: SizeConfig.scaleHeight(133),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: SizeConfig.scaleHeight(6)),
              Container(
                alignment: Alignment.center,
                width: SizeConfig.scaleWidth(width),
                height: SizeConfig.scaleHeight(height),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.BG_ITEM_PRODUCT, //AppColors.BG_ITEM_PRODUCT
                ),
                // child: Image.network(
                //   category.imageUrl,
                //   fit: BoxFit.contain,
                // ),
                child: ProgressiveImage(
                  placeholder: AssetImage('images/image.png'),
                  // size: 1.87KB
                  thumbnail: NetworkImage(category.imageUrl),
                  // size: 1.29MB
                  image: NetworkImage(category.imageUrl),
                  height: SizeConfig.scaleHeight(height - 16),
                  width: SizeConfig.scaleWidth(width - 12),
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(8)),
              MyText(
                title: SharedPrefController().getLangCode == 'en'
                    ? category.nameEn
                    : category.nameAr,
                color: AppColors.TEXT_Bold,
                fontSize: fontTitle,
                height: 1,
                fontWeight: fontWeight,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*
Container(
      alignment: Alignment.center,
      width: SizeConfig.scaleWidth(width),
      height: SizeConfig.scaleHeight(height),
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.scaleWidth(paddingHorizontal),
        vertical: SizeConfig.scaleHeight(
          paddingVertical,
        ),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: bgContainer, //AppColors.BG_ITEM_PRODUCT
      ),
      child: Image.asset(
        image,
        fit: BoxFit.contain,
      ),
    )
* */
