import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MySearchView extends StatelessWidget {
  TextEditingController? searchViewController;
  Color bg_search;
  ValueChanged<String>? onChange;
  String hintSearch;

  MySearchView({
    this.searchViewController,
    this.bg_search = AppColors.WHITE,
    this.onChange,
    this.hintSearch = 'Any food search here',
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      height: SizeConfig.scaleHeight(55),
      decoration: BoxDecoration(
        color: bg_search,
        borderRadius: BorderRadius.circular(6),
      ),
      child: Row(
        children: [
          IconButton(
            icon: Icon(Icons.search, color: AppColors.ICON_SEARCH),
            onPressed: () {},
          ),
          Expanded(
            child: TextField(
              textAlign: TextAlign.start,
              controller: searchViewController,
              onChanged: onChange,
              decoration: InputDecoration(
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                hintText: hintSearch,
                hintStyle: TextStyle(
                  color: AppColors.ICON_SEARCH,
                  fontWeight: FontWeight.w400,
                  fontSize: SizeConfig.scaleTextFont(15),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
