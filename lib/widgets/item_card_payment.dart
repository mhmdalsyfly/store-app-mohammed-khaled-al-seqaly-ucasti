import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/card_payment_controller.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/screens/cart/add_payment_card_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_contianer_shape.dart';
import 'my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ItemCardPayment extends StatelessWidget {
  final CardPayment cardPayment;
  final int index;

  ItemCardPayment({required this.cardPayment, required this.index});

  @override
  Widget build(BuildContext context) {
    return MyContainerShape(
      paddingVertical: 10,
      paddingHorizontal: 15,
      borderRadius: 8,
      width: double.infinity,
      bgContainer: AppColors.WHITE,
      enableShadow: false,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            cardPayment.type.toLowerCase() == 'visa'
                ? 'images/visa_card.png'
                : 'images/master_card.png',
            width: SizeConfig.scaleWidth(44),
            fit: BoxFit.contain,
          ),
          SizedBox(width: SizeConfig.scaleWidth(20)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MyText(
                title: cardPayment.holderName,
                fontSize: 14,
                color: AppColors.TEXT_Bold,
                textAlign: TextAlign.start,
              ),
              SizedBox(height: SizeConfig.scaleHeight(4)),
              MyText(
                title: cardPayment.cardNumber,
                fontSize: 12,
                color: AppColors.LABEL_HINT,
                textAlign: TextAlign.start,
              ),
            ],
          ),
          Spacer(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Material(
                color: AppColors.WHITE,
                borderRadius: BorderRadius.circular(4),
                child: InkWell(
                  onTap: () {
                    Get.to(AddPaymentCardScreen(
                      title: 'Edit Payment Card',
                      cardPayment: cardPayment,
                      index:index
                    ));
                  },
                  borderRadius: BorderRadius.circular(4),
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.scaleWidth(10),
                        vertical: SizeConfig.scaleHeight(5)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MyText(
                          title: AppLocalizations.of(context)!.edit,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(8)),
              Material(
                color: AppColors.WHITE,
                borderRadius: BorderRadius.circular(4),
                child: InkWell(
                  onTap: () async {
                    await CardPaymentController.to
                        .deletePayment(context, cardPayment.id);
                  },
                  borderRadius: BorderRadius.circular(4),
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.scaleWidth(10),
                        vertical: SizeConfig.scaleHeight(5)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MyText(
                          title: AppLocalizations.of(context)!.delete,
                          color: AppColors.RED,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
