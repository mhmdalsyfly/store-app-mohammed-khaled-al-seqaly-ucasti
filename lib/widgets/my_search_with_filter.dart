import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_icon_filter.dart';
import 'my_searchview.dart';

class MySearchWithFilter extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: MySearchView(
            hintSearch: 'Find food for this subcategory',
          ),
        ),
        SizedBox(width: SizeConfig.scaleHeight(14)),
        MyIconFilter()
      ],
    );
  }
}

