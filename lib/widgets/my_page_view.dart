import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_indicator_pageview.dart';

class MyPageView extends StatefulWidget {
  List<String> listImages = [];
  BoxFit boxFit;
  Color color;
  Color colorIndicator;
  double marginBottom;
  bool network;

  MyPageView({
    required this.listImages,
    this.boxFit = BoxFit.cover,
    this.color = AppColors.WHITE,
    this.marginBottom = 10,
    this.network = false,
    this.colorIndicator = AppColors.BG_INDICATOR2,
  });

  @override
  _MyPageViewState createState() => _MyPageViewState();
}

class _MyPageViewState extends State<MyPageView> {
  PageController? _pageController;

  double _viewportFraction = .9;
  double _pageOffset = 0;
  List<Widget> listInd = [];
  int _currentPage = 0;
  late Timer _timer;
  @override
  void initState() {
    _pageController =
        PageController(initialPage: 0, viewportFraction: _viewportFraction)
          ..addListener(() {
            setState(() {
              _pageOffset = _pageController!.page ?? 0;
            });
          });


     _timer= Timer.periodic(Duration(seconds: 2), (Timer timer) {
      if (_currentPage < widget.listImages.length) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
      if (_pageController!.hasClients)
        _pageController!.animateToPage(
          _currentPage,
          duration: Duration(milliseconds: 600),
          curve: Curves.easeIn,
        );
    });

    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    listIndicator();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        PageView.builder(
          itemCount: widget.listImages.length,
          controller: _pageController,
          onPageChanged: (index) {
            selectedImageSlider(index);
          },
          itemBuilder: (context, index) {
            double scale = max(_viewportFraction,
                (1 - (_pageOffset - index).abs()) + _viewportFraction);

            double angle = (_pageOffset - index).abs();
            if (angle > 0.5) {
              angle = 1 - angle;
            }
            return Container(
              padding: EdgeInsetsDirectional.only(
                end: SizeConfig.scaleWidth(10),
                top: 70 - scale * 25,
                bottom: SizeConfig.scaleHeight(widget.marginBottom),
              ),
              child: Transform(
                alignment: Alignment.center,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateY(angle),
                child: Container(
                  color: widget.color,
                  child: Stack(
                    children: [
                      widget.network
                          ? Image.network(
                              widget.listImages[index],
                              width: MediaQuery.of(context).size.width,
                              fit: widget.boxFit,
                            )
                          : Image.asset(
                              widget.listImages[index],
                              width: MediaQuery.of(context).size.width,
                              fit: widget.boxFit,
                            ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        Positioned(
          bottom: SizeConfig.scaleHeight(20),
          left: 0,
          right: 0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: listInd,
          ),
        ),
      ],
    );
  }

  List<Widget> listIndicator() {
    listInd.clear();
    for (int i = 0; i < widget.listImages.length; i++) {
      if (i == 0) {
        listInd.add(MyIndicator(true,widget.colorIndicator));
      } else {
        listInd.add(MyIndicator(false,widget.colorIndicator));
      }
      print(i);
    }
    return listInd;
  }

  selectedImageSlider(int currentSelected) {
    for (int i = 0; i < widget.listImages.length; i++) {
      if (i == currentSelected) {
        listInd[i] = MyIndicator(true,widget.colorIndicator);
      } else {
        listInd[i] = MyIndicator(false,widget.colorIndicator);
      }
    }
  }
}
