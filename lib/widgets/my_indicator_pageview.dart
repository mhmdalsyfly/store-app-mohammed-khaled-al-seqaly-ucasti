import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MyIndicator extends StatelessWidget {
  bool check;
  Color color;

  MyIndicator(this.check,this.color);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(4)),
      height: SizeConfig.scaleHeight(10),
      width: check ? SizeConfig.scaleWidth(47) : SizeConfig.scaleWidth(18),
      decoration: BoxDecoration(
        color: check ? color : AppColors.WHITE,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
