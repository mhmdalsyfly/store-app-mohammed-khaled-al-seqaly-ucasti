import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/utils/app_colors.dart';

class MyFloatingButton extends StatelessWidget {
  String route;
  IconData iconData;
  Color bgColor;

  MyFloatingButton({
    required this.route,
    required this.iconData,
    required this.bgColor,
  });

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(iconData, color: AppColors.WHITE),
      backgroundColor: bgColor,
      onPressed: () {
        Get.toNamed(route);
      },
    );
  }
}

