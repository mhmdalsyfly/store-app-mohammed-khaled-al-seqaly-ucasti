import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

class MyIconFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.WHITE,
      borderRadius: BorderRadius.circular(6),
      child: InkWell(
        onTap: (){},
        borderRadius: BorderRadius.circular(6),
        child: Container(
          clipBehavior: Clip.antiAlias,
          height: SizeConfig.scaleHeight(55),
          width: SizeConfig.scaleWidth(55),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
          ),
          child: Icon(
            Icons.filter_list_outlined,
            color: AppColors.ICON_SEARCH,
          ),
        ),
      ),
    );
  }
}
