import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/address_controller.dart';
import 'package:store_app/model/address.dart';
import 'package:store_app/screens/cart/add_address_screen.dart';
import 'package:store_app/storage/pref/shared_pref_controller.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_text.dart';
import 'my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ItemAddress extends StatelessWidget {
  Address address;
  int index;

  ItemAddress({required this.address, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.scaleWidth(16),
        vertical: SizeConfig.scaleHeight(16),
      ),
      margin: EdgeInsets.symmetric(
        vertical: SizeConfig.scaleHeight(10),
      ),
      decoration: BoxDecoration(
        color: AppColors.WHITE,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: MyText(
              title:
                  address.name + '  -  ' + SharedPrefController().getLangCode ==
                          'en'
                      ? address.city!.nameEn
                      : address.city!.nameAr,
              color: AppColors.TEXT_Bold,
              fontSize: 14,
              textAlign: TextAlign.start,
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(10),
          ),
          SizedBox(
            width: double.infinity,
            child: MyText(
              title: address.info,
              color: AppColors.TEXT_SMALL,
              fontSize: 16,
              textAlign: TextAlign.start,
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(8),
          ),
          SizedBox(
            width: double.infinity,
            child: MyText(
              title: address.contactNumber.toString(),
              color: AppColors.TEXT_Bold,
              fontSize: 16,
              fontWeight: FontWeight.w500,
              textAlign: TextAlign.start,
            ),
          ),
          SizedBox(
            height: SizeConfig.scaleHeight(24),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyTextButton(
                title: AppLocalizations.of(context)!.edit,
                width: 100,
                height: 40,
                enabledSize: true,
                bgColor: AppColors.SELECTED_ITEM.withOpacity(0.1),
                colorText: AppColors.BG_BUTTON,
                fontSize: 14,
                radius: 4,
                fontWeight: FontWeight.normal,
                onPressed: () {
                  Get.to(AddAddressScreen(
                    address: address,
                    index: index,
                  ));
                },
              ),
              MyTextButton(
                title: AppLocalizations.of(context)!.delete,
                width: 100,
                height: 40,
                enabledSize: true,
                radius: 4,
                bgColor: AppColors.BG_BUTTON_DELETE,
                colorText: AppColors.TEXT_Bold,
                fontSize: 14,
                fontWeight: FontWeight.normal,
                onPressed: () {
                  AddressController.to.deleteAddress(context, address.id);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
