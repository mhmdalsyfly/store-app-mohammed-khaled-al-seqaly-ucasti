import 'package:flutter/material.dart';
import 'package:store_app/model/item_profile.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_text.dart';

class MyItemProfile extends StatelessWidget {
  ItemProfile item;
  GestureTapCallback onTap;

  MyItemProfile({
    required this.item,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.BG_SCREENS_HOME,
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Row(
            children: [
              SizedBox(
                width: SizeConfig.scaleWidth(10),
              ),
              Icon(
                item.leading,
                color: AppColors.GRAY,
              ),
              SizedBox(
                width: SizeConfig.scaleWidth(8),
              ),
              Expanded(
                child: MyText(
                  title: item.title,
                  color: AppColors.TEXT_Bold,
                  textAlign: TextAlign.start,
                  fontSize: 18,
                ),
              ),
              Icon(
                item.trailing,
                color: AppColors.GRAY,
                size: 18,
              ),
              SizedBox(
                width: SizeConfig.scaleWidth(10),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
