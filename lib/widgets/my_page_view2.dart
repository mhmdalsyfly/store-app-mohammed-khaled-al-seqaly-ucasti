import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:store_app/model/card_payment.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';
import 'package:store_app/widgets/my_contianer_shape.dart';
import 'package:store_app/widgets/my_indicator_pageview.dart';
import 'package:store_app/widgets/my_text.dart';

class MyPageView2 extends StatefulWidget {
  List<CardPayment> listCardPayment = [];
  BoxFit boxFit;
  Color color;
  Color colorIndicator;
  double marginBottom;
  Function(CardPayment d) function;

  MyPageView2({
    required this.listCardPayment,
    required this.function,
    this.boxFit = BoxFit.cover,
    this.color = AppColors.WHITE,
    this.colorIndicator = AppColors.BG_INDICATOR2,
    this.marginBottom = 10,
  });

  @override
  _MyPageView2State createState() => _MyPageView2State();
}

class _MyPageView2State extends State<MyPageView2> {
  PageController? _pageController;

  double _viewportFraction = .9;
  double _pageOffset = 0;
  List<Widget> listInd = [];

  @override
  void initState() {
    _pageController =
        PageController(initialPage: 0, viewportFraction: _viewportFraction)
          ..addListener(() {
            setState(() {
              _pageOffset = _pageController!.page ?? 0;
            });
          });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    listIndicator();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        PageView.builder(
          itemCount: widget.listCardPayment.length,
          controller: _pageController,
          onPageChanged: (index) {
            print('index $index');
            selectedImageSlider(index);
            widget.function(widget.listCardPayment[index]);
          },
          itemBuilder: (context, index) {
            if(index == 0){
              widget.function(widget.listCardPayment[index]);
            }
            double scale = max(_viewportFraction,
                (1 - (_pageOffset - index).abs()) + _viewportFraction);

            double angle = (_pageOffset - index).abs();
            if (angle > 0.5) {
              angle = 1 - angle;
            }
            return Container(
              padding: EdgeInsetsDirectional.only(
                end: SizeConfig.scaleWidth(10),
                top: 70 - scale * 25,
                bottom: SizeConfig.scaleHeight(widget.marginBottom),
              ),
              child: Transform(
                alignment: Alignment.center,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateY(angle),
                child: MyContainerShape(
                  paddingHorizontal: 32,
                  paddingVertical: 16,
                  enableBorder: false,
                  enableShadow: false,
                  width: double.infinity,
                  bgContainer:
                      widget.listCardPayment[index].type.toLowerCase() == 'visa'
                          ? AppColors.BLUE
                          : AppColors.DARK,
                  borderRadius: 16,
                  child: Column(
                    children: [
                      SizedBox(
                        width: double.infinity,
                      ),
                      Align(
                        alignment: AlignmentDirectional.topEnd,
                        child: Image.asset(
                          widget.listCardPayment[index].type.toLowerCase() ==
                                  'visa'
                              ? 'images/visa_card1.png'
                              : 'images/master_card.png',
                          width: SizeConfig.scaleWidth(44),
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      SizedBox(
                        width: double.infinity,
                        child: MyText(
                          title: widget.listCardPayment[index].cardNumber,
                          color: AppColors.WHITE,
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: MyText(
                          title: widget.listCardPayment[index].cvv.toString(),
                          color: AppColors.WHITE.withOpacity(.9),
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(10)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          MyText(
                            title: widget.listCardPayment[index].holderName,
                            color: AppColors.WHITE,
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            textAlign: TextAlign.start,
                          ),
                          MyText(
                            title: DateFormat('yyyy-MM-dd').format(
                              DateTime.parse(
                                  widget.listCardPayment[index].expDate),
                            ),
                            color: AppColors.WHITE,
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        Positioned(
          bottom: SizeConfig.scaleHeight(20),
          left: 0,
          right: 0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: listInd,
          ),
        ),
      ],
    );
  }

  List<Widget> listIndicator() {
    listInd.clear();
    for (int i = 0; i < widget.listCardPayment.length; i++) {
      if (i == 0) {
        listInd.add(MyIndicator(true, widget.colorIndicator));
      } else {
        listInd.add(MyIndicator(false, widget.colorIndicator));
      }
    }
    return listInd;
  }

  selectedImageSlider(int currentSelected) {
    for (int i = 0; i < widget.listCardPayment.length; i++) {
      if (i == currentSelected) {
        listInd[i] = MyIndicator(true, widget.colorIndicator);
      } else {
        listInd[i] = MyIndicator(false, widget.colorIndicator);
      }
    }
  }
}
