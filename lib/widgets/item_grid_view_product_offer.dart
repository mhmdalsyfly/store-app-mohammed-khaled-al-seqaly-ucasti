import 'package:flutter/material.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/model/product_offer.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'icon_favorit.dart';
import 'my_rating_bar.dart';
import 'my_text.dart';

class ItemGridViewProductOffer extends StatelessWidget {
  ProductOffer productOffer;
  GestureTapCallback onTap;
  double? height;
  double? width;
  int offer;

  ItemGridViewProductOffer({
    required this.productOffer,
    required this.onTap,
    required this.offer,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(5),
          vertical: SizeConfig.scaleHeight(5),
        ),
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.scaleHeight(100),
              child: Stack(
                children: [
                  Container(
                    height: SizeConfig.scaleHeight(100),
                    width: double.infinity,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: AppColors.BG_ITEM_PRODUCT,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Image.network(productOffer.image),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Image.asset(
                      'images/banner_offer.png',
                      width: SizeConfig.scaleWidth(50),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(6)),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(5),
                ),
                child: MyText(
                  title: productOffer.nameEn,
                  color: AppColors.TEXT_Bold,
                  fontSize: 14,
                  textAlign: TextAlign.start,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(5),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    MyText(
                      title: 'Price : ',
                      fontSize: 14,
                      textAlign: TextAlign.start,
                    ),
                    MyText(
                      title: '${productOffer.originalPrice} ₪',
                      color: AppColors.GREEN,
                      fontSize: 15,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      lineThrough: true,
                    ),
                    SizedBox(width: SizeConfig.scaleWidth(8)),
                    MyText(
                      title: '${productOffer.discountPrice} ₪',
                      color: AppColors.GREEN,
                      fontSize: 15,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(5),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    MyText(
                      title: 'End Date : ',
                      fontSize: 14,
                      textAlign: TextAlign.start,
                    ),
                    MyText(
                      title: '${productOffer.endDate}',
                      color: AppColors.GREEN,
                      fontSize: 15,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
