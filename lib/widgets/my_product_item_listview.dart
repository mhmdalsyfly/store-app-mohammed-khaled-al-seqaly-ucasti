import 'package:flutter/material.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/screens/products/product_details_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'item_grid_view_product.dart';

class MyProductItemListView extends StatelessWidget {
  final Product product;
  final double height;
  MyProductItemListView({required this.product,required this.height});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
            color: AppColors.WHITE,
            border: Border.all(
              width: 1,
              color: AppColors.BORDER_Container,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          child: ItemGridViewProduct(
            offer: product.offerPrice == null ? 0 : product.offerPrice!,
            product: product,
            width: SizeConfig.scaleWidth(156),
            height: SizeConfig.scaleHeight(height),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return ProductDetailsScreen(
                      idProduct: product.id.toString(),
                    );
                  },
                ),
              );
            },
          ),
        ),
        SizedBox(
          width: SizeConfig.scaleWidth(14),
        ),
      ],
    );
  }
}
