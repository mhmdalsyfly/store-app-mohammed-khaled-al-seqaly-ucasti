import 'package:flutter/material.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_basket.dart';
import 'number_basket.dart';

class MyBasketFullIcon extends StatelessWidget {
  GestureTapCallback onTap;
  bool enabledMargin;

  double marginTop;
  double marginBottom;
  double marginStart;
  double marginEnd;

  MyBasketFullIcon({
    required this.onTap,
    this.enabledMargin = true,
    this.marginEnd = 20,
    this.marginBottom = 0,
    this.marginTop = 0,
    this.marginStart = 0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: enabledMargin
          ? EdgeInsetsDirectional.only(
              end: SizeConfig.scaleWidth(marginEnd),
              start: SizeConfig.scaleWidth(marginStart),
              top: SizeConfig.scaleHeight(marginTop),
              bottom: SizeConfig.scaleHeight(marginBottom),
            )
          : null,
      alignment: Alignment.center,
      child: MyBasket(
        onTap: onTap,
        child: Stack(
          children: [
            Center(
              child: Image.asset(
                'images/basket.png',
                height: SizeConfig.scaleHeight(26),
                width: SizeConfig.scaleHeight(26),
              ),
            ),
            NumberBasket(),
            SizedBox(
              height: double.infinity,
              width: double.infinity,
            ),
          ],
        ),
      ),
    );
  }
}
