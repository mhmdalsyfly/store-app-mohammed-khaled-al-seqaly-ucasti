import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:store_app/model/get_order.dart';
import 'package:store_app/screens/orders/order_details_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_contianer_shape.dart';
import 'my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderItem extends StatelessWidget {
  GetOrder order;

  OrderItem({required this.order});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.WHITE,
      borderRadius: BorderRadius.circular(8),
      child: InkWell(
        onTap: () {
          Get.to(OrderDetailsScreen(idOrder: order.id));
        },
        borderRadius: BorderRadius.circular(8),
        child: MyContainerShape(
          paddingVertical: 10,
          paddingHorizontal: 16,
          width: double.infinity,
          borderRadius: 8,
          bgContainer: AppColors.TRANSPARENT,
          enableShadow: false,
          enableBorder: false,
          child: Column(
            children: [
              SizedBox(width: double.infinity),
              Row(
                children: [
                  MyText(
                    title: AppLocalizations.of(context)!.numberOrder,
                    textAlign: TextAlign.start,
                    fontSize: 14,
                    height: 1,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(8)),
                  MyText(
                    title: order.orderProductsCount.toString(),
                    color: AppColors.TEXT_Bold,
                  ),
                  Spacer(),
                  MyText(
                    title: order.paymentType,
                    color: AppColors.TEXT_Bold,
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(8)),
              Row(
                children: [
                  MyText(
                    title: AppLocalizations.of(context)!.total,
                    textAlign: TextAlign.start,
                    fontSize: 14,
                    height: 1,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(8)),
                  MyText(
                    title: order.total.toString(),
                    color: AppColors.BG_BUTTON,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(4)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: order.status,
                    color: AppColors.BG_BUTTON,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                  MyText(
                    title: DateFormat('yyyy-MM-dd').format(
                      DateTime.parse(order.date),
                    ),
                    color: AppColors.TEXT_Bold,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
