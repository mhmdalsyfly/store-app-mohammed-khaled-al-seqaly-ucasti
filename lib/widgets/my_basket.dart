import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_text.dart';

class MyBasket extends StatelessWidget {
  Widget child;
  GestureTapCallback onTap;

  MyBasket({
    required this.child,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.scaleWidth(42),
      height: SizeConfig.scaleHeight(42),
      alignment: Alignment.center,
      child: Material(
        borderRadius: BorderRadius.circular(20),
        color: AppColors.WHITE,
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(20),
          child: child,
        ),
      ),
    );
  }
}
