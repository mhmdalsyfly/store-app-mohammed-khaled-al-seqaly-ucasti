import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:store_app/api/controllers_api/cart_controller_getx.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'my_text.dart';

class NumberBasket extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: SizeConfig.scaleHeight(4),
      right: SizeConfig.scaleWidth(4),
      child: Container(
        width: SizeConfig.scaleWidth(14),
        decoration: BoxDecoration(
          color: AppColors.RED,
          shape: BoxShape.circle,
        ),
        child: Obx((){
          print(CartControllerGetx.to.listProducts.toString());
          return MyText(
            title: CartControllerGetx.to.listProducts.length.toString(),
            color: AppColors.WHITE,
            fontSize: 12,
            fontWeight: FontWeight.bold,
          );
        }),
      ),
    );
  }
}
