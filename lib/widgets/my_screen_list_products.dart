import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:store_app/api/controllers_api/categories_controller.dart';
import 'package:store_app/helpers/mixin_sitteings_app.dart';
import 'package:store_app/model/product.dart';
import 'package:store_app/screens/products/product_details_screen.dart';
import 'package:store_app/utils/app_colors.dart';
import 'package:store_app/utils/size_config.dart';

import 'item_grid_view_product.dart';

class MyScreenListProduct extends StatelessWidget with MixinSittingsApp {
  String id;

  MyScreenListProduct({required this.id});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: FutureBuilder<List<Product>>(
        future:
            CategoriesController(context: context).getSubCategoryProducts(id),
        builder: (context, snapshot) {
          List<Product> listProducts = snapshot.data ?? [];
          if (snapshot.hasData) {
            return GridView.builder(
              padding: EdgeInsets.zero,
              itemCount: listProducts.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio:
                    SizeConfig.scaleWidth(156) / SizeConfig.scaleHeight(175),
                crossAxisSpacing: SizeConfig.scaleWidth(14),
                mainAxisSpacing: SizeConfig.scaleHeight(16),
              ),
              itemBuilder: (context, index) {
                int? offer = listProducts[index].offerPrice;
                return ItemGridViewProduct(
                  offer: offer == null ? 0 : offer,
                  product: listProducts[index],
                  onTap: () {
                    Get.to(ProductDetailsScreen(
                      idProduct: listProducts[index].id.toString(),
                    ));
                  },
                );
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
              ),
            );
          }
        },
      ),
    );
  }
}
