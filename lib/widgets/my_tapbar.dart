import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:store_app/utils/app_colors.dart';

class MyTapBar extends StatelessWidget {
  const MyTapBar({
    Key? key,
    required this.listWidgets,
    required this.listTabs,
  }) : super(key: key);

  final List<Widget> listWidgets;
  final List<Tab> listTabs;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: listWidgets.length,
      child: Column(
        children: <Widget>[
          ButtonsTabBar(
            backgroundColor: AppColors.BG_BUTTON.withOpacity(.6),
            unselectedBackgroundColor: Colors.grey[300],
            unselectedLabelStyle:
            TextStyle(color: AppColors.TEXT_Bold),
            labelStyle: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
            tabs: listTabs,
          ),
          Expanded(
            child: TabBarView(
              children: listWidgets,
            ),
          ),
        ],
      ),
    );
  }
}

